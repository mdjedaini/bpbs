#ifndef _LPADAPTER_COIN_OR_CLP_H_
#define _LPADAPTER_COIN_OR_CLP_H_


#include <ClpModel.hpp>
#include <ClpSimplex.hpp>
#include <ClpInterior.hpp>


namespace bbs
{


template<typename SCALAR>
class LPAdapter_COIN_OR_CLP
{

public:
	/*!
     * \brief Default constructor
     *
     * This function is responsible for creating the LP model for a given system, on a given box, and with the LP representation given as template parameter
     *
     */
	LPAdapter_COIN_OR_CLP(void);
	
	/*!
     * \brief Default constructor
     *
     * This function is responsible for creating the LP model for a given system, on a given box, and with the LP representation given as template parameter
     *
     */
	LPAdapter_COIN_OR_CLP(int arg_nbVar, int arg_nbConstraint = 0);

	/*!
     * \brief Copy constructor from pointer
     *
     * This function is responsible for creating the LP model for a given system, on a given box, and with the LP representation given as template parameter
     *
     * \param argp_lpa	: Object to copy
	 *
     */
	LPAdapter_COIN_OR_CLP(LPAdapter_COIN_OR_CLP<SCALAR>* argp_lpa);

	/*!
     * \brief Destructor
     *
     * This function is responsible for creating the LP model for a given system, on a given box, and with the LP representation given as template parameter
	 *
     */
	~LPAdapter_COIN_OR_CLP();

public:
	ClpSimplex* model;	/*!< Pointer to the COINOR LP Model */

public:
	/*!
     * \brief Constructeur
     *
     * This function is responsible for creating the LP model for a given system, on a given box, and with the LP representation given as template parameter
     *
     * \param argp_s		: Pointer on the system for which we want to create the LP model
	 * \param argp_box		: Pointer on the box for which we want to create the LP model
	 *
	 * \return				: Pointer to the LP model created
	 *
     */
	void addVariable(
		std::string name, int arg_id, 
		SCALAR arg_infBound, SCALAR arg_supBound
		);

	/*!
     * \brief Adds a constraint to the model
     *
     * This function is responsible for adding a constraint to the current LP model.
     *
     * \param argr_name		: Name of the constraint
	 * \param arg_id		: Id of the constraint
	 * \param argr_indexes	: Vector of indexes
	 * \param argr_coeffs	: Vector of coefficients
	 * \param arg_gt		: Constraint lower bound
	 * \param arg_lt		: Constraint upper bound
	 *
     */
	void addConstraint(
		std::string argr_name, int arg_id, int arg_type,
		std::vector<int>& argr_indexes, std::vector<SCALAR>& argr_coeffs, 
		SCALAR arg_rhs
		);

	/*!
     * \brief Set the objective
     *
     * Use this function to set the objective function of the current LP model.
     *
     * \param arg_minMax		: -1 for a minimization problem, 1 for a maximization problem
	 * \param argr_indexes		: Vector of indexes
	 * \param argr_coeffs		: Vector of coefficients
	 *
     */
	void setObjective(
		int arg_minMax, 
		std::vector<int>& argr_indexes, 
		std::vector<SCALAR>& argr_coeffs
		);

public:
	/*!
     * \brief Solve the current LP problem.
     *
     * Use this function to launch the solving process.
	 *
     */
	void solve();

public:
	/*!
     * \brief Get the LP status
     *
     * Get the status of the LP after solving.
     *
     * \return		: 1 if optimal
	 *
     */
	int getStatus();

	/*!
     * \brief Get the value of the variable by its id
     *
     * Return the value of the variable with the id given as argument
     *
     * \param arg_id		: Variable id
	 * 
	 * \return				: Value of the variable with the id arg_id
	 *
     */
	SCALAR getValue(int arg_id);

	/*!
     * \brief Get the optimum
     *
     * Get the optimum of the LP after solving. This function is called only if the status is 1.
     *
	 * \return		: LP result
	 *
     */
	SCALAR getOptimum();

}; // class LPAdapter_COIN_OR_CLP


// ######################## //


//
template<typename SCALAR>
LPAdapter_COIN_OR_CLP<SCALAR>::LPAdapter_COIN_OR_CLP(void)
{
	this->model	= new ClpSimplex();
	this->model->setLogLevel(0);
}


//
template<typename SCALAR>
LPAdapter_COIN_OR_CLP<SCALAR>::LPAdapter_COIN_OR_CLP(int arg_nbVar, int arg_nbConstraints)
{
	this->model	= new ClpSimplex();
	this->model->setLogLevel(0);
	this->model->resize(0, arg_nbVar);
}


//
template<typename SCALAR>
LPAdapter_COIN_OR_CLP<SCALAR>::LPAdapter_COIN_OR_CLP(LPAdapter_COIN_OR_CLP* argp_lpa)
{
	this->model	= new ClpSimplex(*(argp_lpa->model));
}


//
template<typename SCALAR>
LPAdapter_COIN_OR_CLP<SCALAR>::~LPAdapter_COIN_OR_CLP(void)
{

}


// a l'appel de cette fonction, le modele global a deja ete cree par le LP factory
// on doit maintenant completer la construction du modele en creant la fonction objectif
// les arguments sont passes a la fonction init plutot que de les restocker en variable membre
template<typename SCALAR>
void LPAdapter_COIN_OR_CLP<SCALAR>::setObjective(int arg_minMax, std::vector<int>& argr_indexes, std::vector<SCALAR>& argr_coeffs)
{
	// 1 -> minimize | -1 -> maximize
	//if(arg_minMax == -1) this->model->setOptimizationDirection(1);
	//if(arg_minMax == 1) this->model->setOptimizationDirection(-1);
	
	if(arg_minMax == -1) this->model->setOptimizationDirection(1);
	if(arg_minMax == 1) this->model->setOptimizationDirection(-1);

	for(unsigned int i = 0; i < argr_indexes.size(); i++)
	{
		// add coeff in objective (id of variable + coefficient)
		this->model->setObjectiveCoefficient(argr_indexes[i], (double)argr_coeffs[i]);
	}

}


//
template<typename SCALAR>
void LPAdapter_COIN_OR_CLP<SCALAR>::addVariable(std::string name, int arg_id, SCALAR arg_infBound, SCALAR arg_supBound)
{
	this->model->setColumnLower(arg_id, (double)arg_infBound);
	this->model->setColumnUpper(arg_id, (double)arg_supBound);
}

//
template<typename SCALAR>
void LPAdapter_COIN_OR_CLP<SCALAR>::addConstraint(
	std::string argr_name, int arg_id, int arg_type,
	std::vector<int>& argr_indexes, std::vector<SCALAR>& argr_coeffs, 
	SCALAR arg_rhs
	)
{

	// number of elements, i.e. # of monomials in the left side of the constraint
	int nb_elem			= argr_coeffs.size();

	// prepare the data to be given to COIN OR
	int* rowIndex		= new int[nb_elem];

	// here there is no templatization, CLP only accept double
	double* rowValue	= new double[nb_elem];

	int cursor	= 0;

	for(int i = 0; i < nb_elem; i++)
	{
		// get the id for the current monomial
		rowIndex[cursor]	= argr_indexes[i];

		// coefficient is first in the pair
		rowValue[cursor]	= (double)argr_coeffs[i];

		// increment cursor
		cursor++;
	}

	if(arg_type == CONSTRAINT_LESS_THAN)
	{
		this->model->addRow(nb_elem, rowIndex, rowValue, -COIN_DBL_MAX, arg_rhs);
	}
	else if(arg_type == CONSTRAINT_MORE_THAN)
	{
		this->model->addRow(nb_elem, rowIndex, rowValue, arg_rhs, COIN_DBL_MAX);
	}
	else if(arg_type == CONSTRAINT_EQUAL_TO)
	{
		this->model->addRow(nb_elem, rowIndex, rowValue, arg_rhs, arg_rhs);
	}

}


// solve with dual simplex
template<typename SCALAR>
void LPAdapter_COIN_OR_CLP<SCALAR>::solve()
{
	//this->model->writeMps("D:\\Users\\mdjedaini\\Documents\\MD_Projects\\PROJET - Contraintes Geometriques\\test\\lp.mps");

	// solving time, we specialize...
	this->model->primal();
}


// return the status (processing, end of process...)
template<typename SCALAR>
int LPAdapter_COIN_OR_CLP<SCALAR>::getStatus()
{
	return this->model->status();
}


// return the status (processing, end of process...)
template<typename SCALAR>
SCALAR LPAdapter_COIN_OR_CLP<SCALAR>::getValue(int arg_id)
{
	//return this->model->getObjValue();
	return *(this->model->getColSolution());
}


// return the optimum (effective result)
template<typename SCALAR>
SCALAR LPAdapter_COIN_OR_CLP<SCALAR>::getOptimum()
{
	return (SCALAR)this->model->getObjValue();
}


} // namespace bbs

#endif

