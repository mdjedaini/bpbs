#ifndef _SYSTEMFACTORY_H_
#define _SYSTEMFACTORY_H_

/*!
 * \file		SystemFactory.h
 * \brief		Programme de tests.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * Programme de test pour l'objet de gestion des cha�nes de caract�res Str_t.
 *
 */

#include "System.h"
#include "mpoly_parser.h"

/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{

/*! 
 * \class	LPFactory
 * \brief	Class responsible for the creation of LPs from the polynomial system
 *
 *  This class is responsible for creating LPs from polynomial systems. It is a kind of interface between these two modules (i.e. Polynomial System and LP modules)
 */
template<typename SCALAR>
class SystemFactory
{

public:
	/*!
     * \brief Deafult constructor
     *
     */
	SystemFactory(void);

	/*!
     * \brief Destructor
     *
     */
	~SystemFactory(void);

public:
	/*!
     * \brief Build internal system from a system file
     *
     * ...
     *
     */
	static System<SCALAR>* buildFromFile(std::string arg_fileName);

	/*!
     * \brief Preprocess file
     *
     * Read and replaces define commands
     *
     */
	static std::string preprocessFile(std::string arg_fileName);

};


// ###################### //


// 
template<typename SCALAR>
SystemFactory<SCALAR>::SystemFactory(void)
{
}


// 
template<typename SCALAR>
SystemFactory<SCALAR>::~SystemFactory(void)
{
}


// 
template<typename SCALAR>
System<SCALAR>* SystemFactory<SCALAR>::buildFromFile(std::string arg_fileName)
{
	System<SCALAR>* res_system	= new System<SCALAR>;

	std::ifstream file;
	std::string line;

	std::cout << "Reading the system file: " << arg_fileName << endl;

	// preprocess the file in memory
	std::string new_fileName	= SystemFactory<SCALAR>::preprocessFile(arg_fileName);

	// open the filestream
	file.open(new_fileName);

	// now, the file is preprocessed
	// the constants have been given their actual value...

	// here, we went back to the beginning of the file after the preprocess
	while (!file.eof())
	{
		std::getline(file, line);
		std::stringstream ss(line);
		std::string token;
		ss >> token;
		
		//std::cout << "Parsing the line: " << line << endl;

		// we are reading a variable declaration
		// unknown x -1 1
		if (token == "unknown")
		{
			std::string varName;
			double lb, ub;
			ss >> varName >> lb >> ub;
			//std::cout << "Variable read " << varName << " - [" << lb << ", " << ub << "]" << endl;
			res_system->addVariable(varName, (SCALAR)lb, (SCALAR)ub);
		}
		
		// At this step, the variables are already added to the system
		else if (token == "0=")
		{
			// parse multivariate polynomial, assume double coefficients
			std::vector<monom_t<SCALAR>> res_local = mpoly_parse<SCALAR>(line.substr(2));
			
			// 
			Multivariate_Polynomial<SCALAR> mp	= Multivariate_Polynomial<SCALAR>();
			
			// on met le cote droit a 0
			SCALAR rightSide = 0;

			for (unsigned int i = 0; i < res_local.size(); i++)
			{
				// monom_tmp.var contains vector of (var, exp)
				monom_t<SCALAR> monom_tmp				= res_local[i];

				// copy to my own implementation
				Multivariate_Monomial<SCALAR> mm_tmp;

				std::vector<std::pair<std::string, int>>::iterator it_varExp;
				for(it_varExp = res_local[i].var.begin(); it_varExp != res_local[i].var.end(); it_varExp++)
				{
					mm_tmp.addFactor(it_varExp->first, it_varExp->second);
				}

				// If monomial is constant, we just send the coeff to the right
				if (mm_tmp.degree == 0)
				{
					rightSide	-= monom_tmp.coeff;
				}

				// else we add the monomial "normally" to the polynomial
				else
				{
					res_system->addMonomial(mm_tmp);
					mp.addMonomial(monom_tmp.coeff, res_system->getMonomialByName(mm_tmp.name));
				}

			}

			// add constraint, deal with constraint name within the function
			res_system->addConstraint("", CONSTRAINT_EQUAL_TO, mp, rightSide);
		}
	}

	// we close the filestream
	file.close();

	// return the system created
	return res_system;
}


// 
template<typename SCALAR>
std::string SystemFactory<SCALAR>::preprocessFile(std::string arg_fileName)
{
	std::string res	= arg_fileName;
	res.replace(res.find(std::string(".system")), std::string(".system").length(), std::string(".system_tmp"));
	
	std::ifstream file;
	std::ofstream new_file;
	std::string line;

	std::cout << "Reading the system file: " << arg_fileName << endl;

	// open the template system file
	file.open(arg_fileName);

	// open the new tmp file created
	new_file.open(res);

	// map of patterns
	std::map<std::string, std::string> pattern_map;

	// we start to preprocess the lines of the file
	while (!file.eof())
	{
		std::getline(file, line);
		std::stringstream ss(line);
		std::string token;
		ss >> token;
		
		// we are reading a variable declaration
		// unknown x -1 1
		if (token == "define")
		{
			std::string key, value;
			ss >> key >> value;

			// add the pattern in the map
			pattern_map[key]	= value;
		}

		// we are reading a variable declaration
		// we copy it to the file
		else if (token == "unknown")
		{
			new_file << line << endl;
		}

		// make the replace in constraints
		else if(token == "0=")
		{
			std::string rightSide	= line.substr(2);

			// iterate over the patterns
			std::map<std::string, std::string>::iterator it_map;
			for(it_map = pattern_map.begin(); it_map != pattern_map.end(); it_map++)
			{
				// start search from index 0
				size_t start_pos = 0;

				// replace all occurences of key by its value
				while((start_pos = rightSide.find(it_map->first, start_pos)) != std::string::npos)
				{
					rightSide.replace(start_pos, it_map->first.length(), it_map->second);
					
					// we update the position to start from
					start_pos += it_map->second.length();
				}
			}

			// write the new line in the file
			new_file << "0=" << rightSide << std::endl;
		}

		// unknown directive
		else
		{
			//std::cout << "I dont know this preprocessor directive" << endl;
		}

	}

	// close the template file
	file.close();

	// close the new file
	new_file.close();

	// return the filename
	return res;
}


} // namespace bbs

#endif