#ifndef _VARIABLE_H_
#define _VARIABLE_H_

/*!
 * \file		LPFactory.h
 * \brief		Programme de tests.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * Programme de test pour l'objet de gestion des cha�nes de caract�res Str_t.
 *
 */

#define INFINI					999999999

#define BOUND_NONE				1
#define BOUND_LOWER				2
#define BOUND_UPPER				3
#define BOUND_LOWER_UPPER		4

namespace bbs
{


// this class manages the variables
template<typename SCALAR>
class Variable
{

public:
	Variable();																					// empty variable
	Variable(std::string arg_name, SCALAR arg_infBound, SCALAR arg_supBound);					// 
	~Variable();																				// 

public:
	std::string name;		/*!< Name of the variable */
	SCALAR infBound;		/*!< Lower bound of the variable */
	SCALAR supBound;		/*!< Upper bound of the variable */
	
public:
	bool operator<( const Variable& other );
	
public:
	/*!
     * \brief Multivariate_Monomial output operator
     *
     * \param argr_o	: Output stream
	 * \param argr_mm	: Monomial to output
	 *
	 * \return			: Output stream
     */
	template<typename SCALAR>
	friend std::ostream& operator<< (std::ostream& argr_o, const Variable<SCALAR>& argr_v);

	/*!
     * \brief Multivariate_Monomial input operator
     *
     * \param argr_i	: Input stream
	 * \param argr_mm	: Monomial to fill in from input stream
	 *
	 * \return			: Input stream
     */
	template<typename SCALAR>
	friend std::istream& operator>> (std::istream& argr_i, Variable<SCALAR>& argr_v);

}; // class Variable


// ###################################### //

template<typename SCALAR>
Variable<SCALAR>::Variable()
{
}


//
template<typename SCALAR>
Variable<SCALAR>::Variable(std::string arg_name, SCALAR arg_infBound, SCALAR arg_supBound)
{
	this->name		= arg_name;
	this->infBound	= arg_infBound;
	this->supBound	= arg_supBound;
}


//
template<typename SCALAR>
Variable<SCALAR>::~Variable()
{
}


//
template<typename SCALAR>
bool Variable<SCALAR>::operator<( const Variable& other )
{
	return this->name < other.name;
}


//
template<typename SCALAR>
std::ostream& operator<< (std::ostream& argr_o, const Variable<SCALAR>& argr_v)
{
	argr_o << argr_v.name << " - [ " << (argr_v.infBound) << " , " << (argr_v.supBound) << " ]";

	return argr_o;
}


// nothing special to do...
template<typename SCALAR>
std::istream& operator>> (std::istream& argr_i, Variable<SCALAR>& argr_v)
{
	return argr_i;
}


} // namespace bbs

#endif