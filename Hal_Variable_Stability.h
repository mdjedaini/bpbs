#ifndef _HAL_VARIABLE_STABILITY_H_
#define _HAL_VARIABLE_STABILITY_H_

#include "Box.h"

namespace bbs
{


template<typename SCALAR>
class Hal_Variable_Stability
{

public:
	Hal_Variable_Stability(void);
	~Hal_Variable_Stability(void);

public:
	bool operator()(Box<SCALAR>* b);

};


// ############################### //


//
template<typename SCALAR>
Hal_Variable_Stability<SCALAR>::Hal_Variable_Stability(void)
{
}


//
template<typename SCALAR>
Hal_Variable_Stability<SCALAR>::~Hal_Variable_Stability(void)
{
}


// do we have to split, or not?
template<typename SCALAR>
bool Hal_Variable_Stability<SCALAR>::operator()(Box<SCALAR>* argp_b)
{
	for(int i = 0; i < argp_b->nb_var; i++)
	{
		// if at least one variable is not stable we do not split
		if(argp_b->lower_stability[i] == 0 || argp_b->upper_stability[i] == 0)
		{
			return true;
		}
	}

	return false;

}


} // namespace bbs

#endif

