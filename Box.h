#ifndef _BOX_H_
#define _BOX_H_

/*!
 * \file		Box.h
 * \brief		File containing the Box class. which contains the variable bounds computed at each iteration.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * File containing the Box class, which contains the variable bounds computed at each iteration.
 *
 */

#include "System.h"
#include "Box_Params.h"

/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{

/*! 
 * \class	Box
 * \brief	Box class contains the bounds of system variables, computed at each iteration.
 *
 * 
 */
template<typename SCALAR>
class Box
{

public:
	/*!
     * \brief Default constructor
     *
     * Default constructor creates an empty box.
     */
	Box();

	/*!
     * \brief Copy constructor
     *
     * Creates a copy of the provided box.
     *
	 * \param argp_b		: Pointer to the box to copy
	 *
     */
	Box(Box* argp_b);

	/*!
     * \brief Constructor from system
     *
     * Creates a box from a given system, and initialize the box bounds from the system variables.
     *
     * \param argp_s		: Pointer to the system from which we create the box
	 *
     */
	Box(System<SCALAR>* argp_s, Box_Params<SCALAR>* argp_box_params);

	/*!
     * \brief Destructor
	 *
     * The descrutor does not delete member pointers.
	 * System still requires these values after deleting the box.
     */
	~Box();

public:

	/*!
     * \brief Dimension of the box, i.e. number of variables
     *
     */
	System<SCALAR>* s;

	/*!
     * \brief Dimension of the box, i.e. number of variables
     *
     */
	int nb_var;

	/*!
     * \brief Dimension of the box, i.e. number of variables
     *
     */
	Box_Params<SCALAR>* box_params;

	/*!
     * \brief Variables lower bounds. Variable is identified by its position in the vector
     *
     */
	std::vector<SCALAR> lower;
	
	/*!
     * \brief Variables upper bounds. Variable is identified by its position in the vector
     *
     */
	std::vector<SCALAR> upper;
	
	/*!
     * \brief Variables intervals widths. Variable is identified by its position in the vector
     *
     */
	std::vector<SCALAR> width;

	/*!
     * \brief Variable lower bound stability. Variable is identified by its position in the vector
     *
     */
	std::vector<int> lower_stability;
	
	/*!
     * \brief Variable upper bound stability. Variable is identified by its position in the vector
     *
     */
	std::vector<int> upper_stability;

public:
	
	/*!
     * \brief Checks whether a box is a solution or not.
     *
     * Checks whether a box is a solution or not. A box is a solution if all its ranges are less than a given value epsilon.
	 *
	 * \return	: true if the box is a solution, false if not
	 *
     */
	bool isSolution();

	/*!
     * \brief Updates one bound of the box
     *
     * Responsible for updating a given bound of a given variable in the box.
     *
     * \param arg_minMax		: -1 to update the lower bound, 1 to update the upper bound
	 * \param arg_varName		: Name of the variable to update
	 * \param arg_value			: Value of the new bound
	 *
     */
	void updateBound(int arg_minMax, std::string arg_varName, SCALAR arg_value);

	/*!
     * \brief Updates all bounds of the box
     *
     * Responsible for updating all the bounds of the box, and replace them with the provided bounds.
     *
     * \param argr_newLower	: New lower bounds, indexed by variables Ids
	 * \param argp_newUpper	: New upper bounds, indexed by variables Ids
	 *
     */
	void updateBounds(std::vector<SCALAR>& argr_newLower, std::vector<SCALAR>& argr_newUpper);

	/*!
     * \brief Get the list of variables to shrink
     *
     * Returns a list of all variables to shrink.
	 * Currently, the variable which require shrink are the stable variable with a range < epsilon.
	 *
	 * \return				: Vector of variables that require a shrink
	 *
     */
	std::vector<std::string> getVariablesToShrink();

	/*!
     * \brief Get the list of stable variables
     *
     * Returns a list of all stable variables of the box.
	 * Stable variables are variables for which bounds have remained the same for at least one iteration.
	 *
	 * \return				: Vector containing names of stable variables
	 *
     */
	std::vector<std::string> getStableVariables();

public:
	/*!
     * \brief Box equality test operator
	 *
	 * Box equality test operator
	 *
	 * \return				: true if the box given in argument is equal to the current box, false otherwise
	 *
     */
	bool operator==( const Box& other ) const;

public:
	/*!
     * \brief Box output operator
     *
	 * Output a box to the output stream.
	 * 
	 * \return				: Output stream
	 *
     */
	template<typename SCALAR>
	friend std::ostream& operator<< (std::ostream& argr_o, const Box<SCALAR>& argr_b);

	/*!
     * \brief Box output operator
     *
	 * Output a box to the output stream.
	 * 
	 * \return				: Output stream
	 *
     */
	template<typename SCALAR>
	friend std::istream& operator>> (std::istream& argr_i, Box<SCALAR>& argr_b);        // input operator

};


// ####################### //


//
template<typename SCALAR>
Box<SCALAR>::Box(void)
{

}


//
template<typename SCALAR>
Box<SCALAR>::Box(Box* argp_b)
{
	this->s						= argp_b->s;
	this->box_params			= argp_b->box_params;
	this->nb_var				= argp_b->nb_var;

	this->box_params->vToC					= argp_b->box_params->vToC;
	this->box_params->cToV					= argp_b->box_params->cToV;

	this->lower	= std::vector<SCALAR>(argp_b->lower);
	this->upper	= std::vector<SCALAR>(argp_b->upper);
	this->width	= std::vector<SCALAR>(argp_b->width);

	this->lower_stability	= std::vector<int>(this->nb_var);
	this->upper_stability	= std::vector<int>(this->nb_var);
}


//
template<typename SCALAR>
Box<SCALAR>::Box(System<SCALAR>* argp_s, Box_Params<SCALAR>* argp_box_params)
{
	this->s				= argp_s;
	this->box_params	= argp_box_params;
	this->nb_var		= argp_s->nb_var;

	this->box_params->vToC		= &(argp_s->vToC);
	this->box_params->cToV		= &(argp_s->cToV);

	this->lower		= std::vector<SCALAR>(this->nb_var);
	this->upper		= std::vector<SCALAR>(this->nb_var);
	this->width		= std::vector<SCALAR>(this->nb_var);

	this->lower_stability	= std::vector<int>(this->nb_var);
	this->upper_stability	= std::vector<int>(this->nb_var);

	// import variable bounds for the system
	std::map<int, Variable<SCALAR>>::iterator it_v;
	for(it_v = argp_s->v_list.begin(); it_v != argp_s->v_list.end(); it_v++)
	{
		// -1 to arrange indices for vector
		int varId	= this->s->vToC[it_v->second.name];

		this->lower[varId]	= it_v->second.infBound;
		this->upper[varId]	= it_v->second.supBound;

		this->width[varId]	= abs(this->upper[varId] - this->lower[varId]);
	}

}


//
template<typename SCALAR>
Box<SCALAR>::~Box(void)
{

}


// check if all variables have the required accuracy
template<typename SCALAR>
bool Box<SCALAR>::isSolution()
{
	// if at least one width > epsilon we return false
	for(int i = 0; i < this->nb_var; i++)
	{
		if(this->width[i] > *(this->box_params->epsilon))
		{
			return false;
		}
	}

	// else return true
	return true;
}


// update bounds of the box
// arg_minMax = -1 for min and 1 for max
template<typename SCALAR>
void Box<SCALAR>::updateBound(int arg_minMax, std::string arg_varName, SCALAR arg_value)
{
	// get column
	int varColumn	= this->box_params->vToC->at(arg_varName);
	
	// if width <= epsilon, then the variable is a solution and no need to shrink it
	// we just consider it stable and return
	if(this->width[varColumn] <= *(this->box_params->epsilon))
	{
		this->lower_stability[varColumn]++;
		this->upper_stability[varColumn]++;
		return;
	}

	// set the minimum bound
	if(arg_minMax == -1)
	{
		// TODO faire gaffe a la division par 0!!!
		// shrinking percentage, i.e. how much the bound is shrinked in percentage
		double shrink_percentage	= abs((double)arg_value - (double)this->lower[varColumn]) * 100 / (double)this->width[varColumn];

		// we added the case < to avoid to have larger intervals
		if(shrink_percentage <= *(this->box_params->stability_criteria))
		{
			this->lower_stability[varColumn]++;
		}
		else
		{
			this->lower[varColumn]				= arg_value;
			this->lower_stability[varColumn]	= 0;
		}
	}

	// set the maximum bound
	else if(arg_minMax == 1)
	{
		// shrinking percentage, i.e. how much the bound is shrinked in percentage
		//double shrink_percentage	= abs((double)arg_value - (double)this->lower[varColumn]) / (double)this->width[varColumn];
		double shrink_percentage	= abs((double)this->upper[varColumn] - (double)arg_value) * 100 / (double)this->width[varColumn];

		// we added the case > to avoid to have bigger bounds
		if(shrink_percentage <= *(this->box_params->stability_criteria))
		{
			this->upper_stability[varColumn]++;
		}
		else
		{
			this->upper[varColumn]				= arg_value;
			this->upper_stability[varColumn]	= 0;
		}
	}

	// update intervals width
	this->width[varColumn]	= abs(this->upper[varColumn] - this->lower[varColumn]);
}


//
template<typename SCALAR>
void Box<SCALAR>::updateBounds(std::vector<SCALAR>& argr_newLower, std::vector<SCALAR>& argr_newUpper)
{
	// for each variable
	for(int i = 0; i < this->nb_var; i++)
	{
		// update the lower bound of the current variable
		this->updateBound(-1, this->s->cToV[i], argr_newLower[i]);

		// update the upper bound of the current variable
		this->updateBound(1, this->s->cToV[i], argr_newUpper[i]);
	}
}


// return a vector of variables to shrink
template<typename SCALAR>
std::vector<std::string> Box<SCALAR>::getVariablesToShrink()
{
	// result, vector of variable names
	std::vector<std::string> res;

	// put all variables for which width > epsilon
	for(int i = 0; i < this->nb_var; i++)
	{
		if(this->width[i] > *(this->box_params->epsilon))
		{
			res.push_back(this->box_params->cToV->at(i));
		}
	}
	
	return res;
}


// return a vector of variables to shrink
template<typename SCALAR>
std::vector<std::string> Box<SCALAR>::getStableVariables()
{
	std::vector<std::string> res;

	for(int i = 0; i < this->nb_var; i++)
	{
		if(	this->lower_stability[i] > 0 && this->upper_stability[i] > 0 && this->width[i] > *(this->epsilon))
		{
			res.push_back(this->cToV->at(i));
		}
	}

	return res;
}


//
template<typename SCALAR>
bool Box<SCALAR>::operator==(const Box& other) const
{
	//
	for(int i = 0; i < this->nb_var; i++)
	{
		/*
		// we use == operator instead of !=
		if	(abs(this->lower[i] - other.lower[i]) > *(this->box_params->precision) || abs(this->upper[i] - other.upper[i]) > *(this->box_params->precision))
		{
			return false;
		}
		*/
		// we use == operator instead of !=
		if	(abs(this->lower[i] - other.lower[i]) > *(this->box_params->epsilon) || abs(this->upper[i] - other.upper[i]) > *(this->box_params->epsilon))
		{
			return false;
		}
	}

	return true;
}


// print a system
template<typename SCALAR>
std::ostream& operator<< (std::ostream& argr_o, const Box<SCALAR>& argr_b)
{
	// print variables
	argr_o << "Box:" << std::endl;
	argr_o << "__________________________" << std::endl;

	argr_o << endl;

	// print each variable indepdendently
	for(int i = 0; i < argr_b.nb_var; i++)
	{
		argr_o << argr_b.s->cToV[i] << " - [" << argr_b.lower[i] << ", " << argr_b.upper[i] << "]" << std::endl;
	}

	return argr_o;
}


// nothing special to do...
template<typename SCALAR>
std::istream& operator>> (std::istream& argr_i, Box<SCALAR>& argr_b)
{
	return argr_i;
}


} // namespace bbs

#endif