#ifndef _BBS_SOLVER_H_
#define _BBS_SOLVER_H_

/*!
 * \file		BBS_Solver.h
 * \brief		File containing the BBS_Solver class, responsible for solving systems.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * File containing the BBS_Solver class, responsible for solving systems.
 *
 */

#include "System.h"
#include "Solution.h"
#include "Box.h"
#include "LPFactory.h"
#include "Params.h"


/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{

/*! 
 * \class	BBS_Solver
 * \brief	BBS_Solver class is responsible for solving the system it contains as a member variable.
 *
 * BBS_Solver is responsible for solving multivariate polynomials systems.
 * The system is given as a member variable of this class.
 * The Solution member is filled throughout the solving process.
 */
template<typename SCALAR, typename SOLUTION_ISOLATION_STRATEGY, typename LP_FACTORY>
class BBS_Solver
{

public:
typedef	SCALAR	td_scalar;

//
public:
	/*!
     * \brief Default constructor
     *
     * Default constructor creates an empty BBS_Solver object.
	 *
     */
	BBS_Solver();

	/*!
     * \brief Constructor from System
	 *
	 * Creates a BBS_Solver object containing the given system.
     *
     * \param argp_s		: Pointer on the system we want to solve
	 *
     */
	BBS_Solver(System<SCALAR>* argp_s, Params<SCALAR, SOLUTION_ISOLATION_STRATEGY, LP_FACTORY>* argp_params);

	/*!
     * \brief Destructor
	 *
	 * The destructor deletes the System, the LP_Factory and the Solution.
	 * To use them after the solving process, make a copy of them.
	 *
     */
	~BBS_Solver();

public:
	/*!
     * \brief Pointer to the system to solve
	 *
     */
	System<SCALAR>* s;

	/*!
     * \brief Pointer to the solution of the current system
	 *
     */
	Solution<SCALAR>* sol;

	/*!
     * \brief Pointer to the LPFactory which is used to solve
	 *
     */
	Params<SCALAR, SOLUTION_ISOLATION_STRATEGY, LP_FACTORY>* params;

	/*!
     * \brief Indicates whether to pause the solving process or not.
	 *
     */
	bool m_pause;

	/*!
     * \brief Indicates whether to stop the solving process or not.
	 *
     */
	bool m_stop;

//
public:
	/*!
     * \brief Responsible for solving a system from scratch.
     *
     * Responsible for solving a system from scratch.
	 * It creates the starting box, and solves it iteratively.
	 *
     */
	void solveSystem();

	/*!
     * \brief Responsible for reducing a given box.
     *
     * Responsible for reducing a given box.
	 * It shrinks it iteratively, and performs splits if necessary.
     *
     * \param argp_b		: Pointer to the box to reduce
	 *
     */
	void solveBox(Box<SCALAR>* argp_b);

	/*!
     * \brief Pause the solving process
     *
     * Responsible for reducing a given box.
	 * It shrinks it iteratively, and performs splits if necessary.
	 *
     */
	void pause();

	/*!
     * \brief Stops the solving process and returns.
     *
     * Responsible for reducing a given box.
	 * It shrinks it iteratively, and performs splits if necessary.
	 *
     */
	void stop();

};


// ####################### //


//
template<typename SCALAR, typename SOLUTION_ISOLATION_STRATEGY, typename LP_FACTORY>
BBS_Solver<SCALAR, SOLUTION_ISOLATION_STRATEGY, LP_FACTORY>::BBS_Solver()
{

}


//
template<typename SCALAR, typename SOLUTION_ISOLATION_STRATEGY, typename LP_FACTORY>
BBS_Solver<SCALAR, SOLUTION_ISOLATION_STRATEGY, LP_FACTORY>::BBS_Solver(System<SCALAR>* argp_s, Params<SCALAR, SOLUTION_ISOLATION_STRATEGY, LP_FACTORY>* argp_params)
{
	this->s			= argp_s;
	this->params	= argp_params;

	// create the lp factory here instead of the params
	this->params->lpf	= new lpf_type();

	this->sol		= new Solution<SCALAR>(&(this->params->precision));
	this->m_pause	= false;
	this->m_stop	= false;
}


//
template<typename SCALAR, typename SOLUTION_ISOLATION_STRATEGY, typename LP_FACTORY>
BBS_Solver<SCALAR, SOLUTION_ISOLATION_STRATEGY, LP_FACTORY>::~BBS_Solver()
{
	// delete the lp factory
	delete this->params->lpf;
}


//
template<typename SCALAR, typename SOLUTION_ISOLATION_STRATEGY, typename LP_FACTORY>
void BBS_Solver<SCALAR, SOLUTION_ISOLATION_STRATEGY, LP_FACTORY>::solveSystem()
{
	// create box parameters
	Box_Params<SCALAR>* box_params	= new Box_Params<SCALAR>();
	box_params->epsilon				= &(this->params->wanted_accuracy);
	box_params->precision			= &(this->params->precision);
	box_params->stability_criteria	= &(this->params->stability_criteria);

	// create starting box
	Box<SCALAR>* starting_box	= new Box<SCALAR>(this->s, box_params);

	// create a pointer to the logger for easy manipulation
	Logger<SCALAR>* tmp_logger	= this->params->logger;

	// log the starting box
	tmp_logger->log(LOG_DATA_STARTING_BOX, starting_box);

	// start the chronometer
	this->params->chrono->init();
	tmp_logger->log(LOG_TIME, this->params->chrono->getElapsedTime());

	// solve the starting box, epsilon is in the box
	this->solveBox(starting_box);

	//
	tmp_logger->log(LOG_MESSAGE_ADMINISTRATIVE, std::string("FINISH! Click on the Reset button to reset all data..."));
}


//
template<typename SCALAR, typename SOLUTION_ISOLATION_STRATEGY, typename LP_FACTORY>
void BBS_Solver<SCALAR, SOLUTION_ISOLATION_STRATEGY, LP_FACTORY>::solveBox(Box<SCALAR>* argp_b)
{	

	// create a pointer to the logger for easy manipulation
	Logger<SCALAR>* tmp_logger	= this->params->logger;

	// pick up the time
	tmp_logger->log(LOG_TIME, this->params->chrono->getElapsedTime());

	// returns if the m_stop flag is set to true
	if(this->m_stop == true)
	{
		tmp_logger->log(LOG_MESSAGE_SOLVER_INFORMATION, std::string("Solving process stopped manually...\r\n"));
		//tmp_logger << "Solving process stopped manually..." << std::endl;
		return;
	}

	tmp_logger->log(LOG_MESSAGE_SOLVER_INFORMATION, std::string("Solving the box..."));
	tmp_logger->log(bbs::LOG_DATA_CURRENT_BOX, argp_b);

	// we shrink the box
	int shrink_status	= this->params->sis->shrinker->operator()(this->s, argp_b, this->params->lpf);

	// pick up the time
	tmp_logger->log(LOG_TIME, this->params->chrono->getElapsedTime());

	// box to solve
	tmp_logger->log(LOG_MESSAGE_SOLVER_INFORMATION, std::string("Box after shrink..."));
	tmp_logger->log(LOG_DATA_BOX_AFTER_SHRINK, argp_b);

	//cout.str();

	// deal with boxes without solution
	if(shrink_status == 0)
	{
		// 
		tmp_logger->log(LOG_DATA_NEW_EMPTY_BOX, argp_b);

		delete argp_b;
		return;
	}


	// if the box is a solution we add it
	if(argp_b->isSolution())
	{
		tmp_logger->log(LOG_MESSAGE_SOLVER_INFORMATION, std::string("Solution box!"));
		tmp_logger->log(LOG_MESSAGE_SOLVER_INFORMATION, std::string("Now analyzing the solution..."));

		tmp_logger->log(LOG_DATA_NEW_SOLUTION_BOX, argp_b);

		// add the box to the solution vector
		int sol_ou_pas_sol	= this->sol->addBox(argp_b);

		if(sol_ou_pas_sol == 0)
		{
			// solution goes to garbage
			tmp_logger->log(LOG_MESSAGE_SOLVER_INFORMATION, std::string("Unfortunately, this box goes to garbage...\r\n"));
			tmp_logger->log(LOG_DATA_NEW_GARBAGE_BOX, argp_b);
		}

		else
		{
			tmp_logger->log(LOG_MESSAGE_SOLVER_INFORMATION, std::string("Box is ok! There is ") + std::to_string((long double)this->sol->sol.size()) + std::string(" solutions up to now...\r\n"));
			tmp_logger->log(LOG_DATA_NEW_FINAL_SOLUTION_BOX, argp_b);
		}
		
		// delete the box
		delete argp_b;

		// finish the function
		return;
	}

	// here, the box is not a solution, we check if we have to split it or not

	// pick up the time
	tmp_logger->log(LOG_TIME, this->params->chrono->getElapsedTime());

	// return true if we have to split, false if not
	if(this->params->sis->hal->operator()(argp_b))
	{
		// get the indices where to split
		// in most of the cases, it is only one value, but it can be several values
		std::vector<std::string> i					= this->params->sis->ayn->operator()(argp_b);

		tmp_logger->log(LOG_MESSAGE_SOLVER_INFORMATION, std::string("Box splitting selon la variable: ") + i[0]);
		
		// effectively split the current box using our kayf object
		std::vector<Box<SCALAR>*> boxes_to_solve	= this->params->sis->kayf->operator()(argp_b, i, this->params->box_gap);

		// provide the two child boxes to the logger
		tmp_logger->log(LOG_EVENT_NEW_SPLITTING, boxes_to_solve[0], boxes_to_solve[1], i[0]);

		// destroy the previous box
		delete argp_b;

		// PARALLELISER
		// solve all the boxes
		std::vector<Box<SCALAR>*>::iterator it_box;
		for(it_box = boxes_to_solve.begin(); it_box != boxes_to_solve.end(); it_box++)
		{
			// solve each box
			this->solveBox((*it_box));
		}
	}

	// the box is not yet a solution, but we do not have to split it, so we continue to shrink it
	else
	{
		// BE CAREFUL, perform the log before entering in the solving process
		tmp_logger->log(LOG_DATA_ITERATE_ON_SAME_BOX, argp_b);

		this->solveBox(argp_b);
	}

}


//
template<typename SCALAR, typename SOLUTION_ISOLATION_STRATEGY, typename LP_FACTORY>
void BBS_Solver<SCALAR, SOLUTION_ISOLATION_STRATEGY, LP_FACTORY>::pause()
{
	this->m_pause	= true;
}

//
template<typename SCALAR, typename SOLUTION_ISOLATION_STRATEGY, typename LP_FACTORY>
void BBS_Solver<SCALAR, SOLUTION_ISOLATION_STRATEGY, LP_FACTORY>::stop()
{
	this->m_stop	= true;
}


} // namespace bbs


#endif