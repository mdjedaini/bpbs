// Simple Multivariate Polynomial Parser
// (c) George M. Tzoumas, LE2I, Université de Bourgogne, Dijon, France, 2012
//
// portions (c) Christoph Fuenfzig, LE2I, Université de Bourgogne, Dijon, France, 2009.
#include "stdafx.h"
#include "mpoly_parser.h"


Symbolic expanded_power(Symbolic x, int k)
{
    if (k == 1) return x;
    if (k == 0) return Symbolic(1);
    if (k % 2 == 0) {
        Symbolic y = expanded_power(x, k/2);
        return y*y;
    } else {
        Symbolic y = expanded_power(x, (k-1)/2);
        return y*y*x;
    }
}


bool mpoly_symbolic_parse (const std::string &input, Symbolic &s)
{
    expression my_exp;
    tree_parse_info<const char*> tree;
    state_t res;

    // Most errors can be caught in the parse with parser exceptions
    try
	{
		tree = ast_parse(input.c_str(), my_exp, space_p);
	}
	catch (parser_error<Errors,char const*> x)
	{
		switch (x.descriptor)
		{
		case close_expected:
			std::cerr << "Expected close parenthesis" << std::endl;
			break;
		case expression_expected:
			std::cerr << "Expected operand" << std::endl;
			break;
#ifdef CALC_DETECT_CONSECUTIVE_OPERANDS
		case binary_operator_expected:
			std::cerr << "Consecutive Operands Not Allowed" << std::endl;
			break;
#endif
		}
      std::cerr << "-->| " << input << " at position " << std::distance(input.c_str(), x.where)+1 << std::endl;
      return false;
    }

    if (tree.full)
	{ // Did the parse complete:
      try
	  {
        res = parse_subtree(tree.trees.begin());
        s = res.third;
        return true;
//        return get_monomials<double>(res.third);
      }
	  catch (std::runtime_error& e)
	  {
        std::cerr << e.what() << std::endl;
        return false;
      }
    }

    // Or was the expression rejected?
    // But open parenthesis and missing operators are very hard to
    // check for during the parse, so it is easier to check
    // for an incomplete parse afterwards:
    switch (*tree.stop)
	{
    case ')':
      std::cerr << "Expected open parenthesis\n";
      break;
    case '(':
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
      std::cerr << "Expected binary operator\n";
      break;
    default:
      std::cerr << "Not a number\n";
    }

    std::cerr << "-->| " << input << " at position " << std::distance(input.c_str(),tree.stop)+1 << std::endl;
    return false;
}

