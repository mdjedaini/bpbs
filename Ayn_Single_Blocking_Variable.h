#ifndef _AYN_SINGLE_BLOCKING_VARIABLE_H_
#define _AYN_SINGLE_BLOCKING_VARIABLE_H_

#include "Box.h"

namespace bbs
{


template<typename SCALAR>
class Ayn_Single_Blocking_Variable
{

public:
	Ayn_Single_Blocking_Variable(void);
	~Ayn_Single_Blocking_Variable(void);

public:
	std::vector<std::string> operator()(Box<SCALAR>* argp_b);

};


// ######################### //


//
template<typename SCALAR>
Ayn_Single_Blocking_Variable<SCALAR>::Ayn_Single_Blocking_Variable(void)
{
}


//
template<typename SCALAR>
Ayn_Single_Blocking_Variable<SCALAR>::~Ayn_Single_Blocking_Variable(void)
{
}


// where to split? give a list of indexes (in most of the case just one index)
template<typename SCALAR>
std::vector<std::string> Ayn_Single_Blocking_Variable<SCALAR>::operator()(Box<SCALAR>* argp_b)
{
	std::vector<std::string> res;

	// get the variables to shrink
	std::vector<std::string> vts	= argp_b->getVariablesToShrink();

	//
	std::vector<std::string>::iterator it_vts;
	
	// if we dont find one variable stable, we split
	for(it_vts = vts.begin(); it_vts != vts.end(); it_vts++)
	{
		// get column of the variable
		int i	= argp_b->s->vToC[*it_vts];

		// 
		if(argp_b->lower_stability[i] >= 1 && argp_b->upper_stability[i] >= 1)
		{
			res.push_back(*it_vts);
			break;
		}
	}

	return res;
}


} // namespace bbs


#endif
