#ifndef _LOGGER_H_
#define _LOGGER_H_

/*!
 * \file		LPFactory.h
 * \brief		Programme de tests.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * Programme de test pour l'objet de gestion des cha�nes de caract�res Str_t.
 *
 */

#include "Box.h"
#include "Solution.h"
 
/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{

/*! 
 * \brief	Logger class is responsible for logging events occuring within the solving process.
 *
 * 
 */
typedef enum
{
	
	LOG_MESSAGE_ADMINISTRATIVE,
	LOG_MESSAGE_SOLVER_INFORMATION,

	LOG_DATA_STARTING_BOX,
	LOG_DATA_CURRENT_BOX,
	LOG_DATA_BOX_AFTER_SHRINK,
	LOG_DATA_NEW_EMPTY_BOX,
	LOG_DATA_NEW_SOLUTION_BOX,
	LOG_DATA_NEW_FINAL_SOLUTION_BOX,
	LOG_DATA_NEW_GARBAGE_BOX,
	LOG_DATA_ITERATE_ON_SAME_BOX,
	LOG_DATA_FINAL_SOLUTIONS,

	LOG_EVENT_NEW_SPLITTING,

	LOG_TIME

} Log_Type;


/*! 
 * \class	Logger
 * \brief	Logger class is responsible for logging events occuring within the solving process.
 *
 * 
 */
template<typename SCALAR>
class Logger
{

public:
	/*!
     * \brief Default constructor
	 *
	 */
	Logger();


	/*!
     * \brief Destructor
	 *
     */
	~Logger();

public:

	/*!
     * \brief Custom output stream
	 *
	 * 
	 * 
     */
	Log_Type log_type;

public:
	/*!
     * \brief Logs a message
	 *
	 * 
	 * 
     */
	virtual void log(Log_Type arg_type, ...);

};


//


//
template<typename SCALAR>
Logger<SCALAR>::Logger()
{
	this->log_type	= bbs::LOG_MESSAGE_ADMINISTRATIVE;
}


//
template<typename SCALAR>
Logger<SCALAR>::~Logger()
{

}


//
template<typename SCALAR>
void Logger<SCALAR>::log(Log_Type arg_logType, ...)
{
	this->log_type		= arg_logType;

	// liste des arguments
	va_list ap;

	// gives the last known parameter
    va_start(ap, arg_logType);

	if(arg_logType == bbs::LOG_MESSAGE_ADMINISTRATIVE)
	{

	}

	//
	else if(arg_logType == bbs::LOG_MESSAGE_SOLVER_INFORMATION)
	{
		//Requires the type to cast to. Increments ap to the next argument.
		std::string logText	= va_arg(ap, std::string);
		std::cout << logText;
	}

	//
	else if(arg_logType == bbs::LOG_DATA_CURRENT_BOX)
	{
		//Requires the type to cast to. Increments ap to the next argument.
		Box<SCALAR>* logBox	= va_arg(ap, Box<SCALAR>*);
		std::cout << (*logBox);
	}

	//
	else if(arg_logType == bbs::LOG_DATA_BOX_AFTER_SHRINK)
	{
		//Requires the type to cast to. Increments ap to the next argument.
		Box<SCALAR>* logBox	= va_arg(ap, Box<SCALAR>*);
		std::cout << (*logBox);
	}

	//
	else if(arg_logType == bbs::LOG_DATA_FINAL_SOLUTIONS)
	{
		//Requires the type to cast to. Increments ap to the next argument.
		Solution<SCALAR>* logSolution	= va_arg(ap, Solution<SCALAR>*);
		std::cout << (*logSolution);
	}

	else
	{
		std::cout << "Type de log non defini..." << std::endl;
	}

    va_end(ap);
}


} // namespace


#endif