#ifndef _BERNSTEIN_H_
#define _BERNSTEIN_H_

/*!
 * \file		Bernstein.h
 * \brief		File containing the Bernstein class, responsible for generating Bernstein polytopes / constraints.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * File containing the Bernstein class, responsible for generating Bernstein polytopes / constraints for monomials on a given box.
 *
 */

#include "System.h"
#include "Constraint.h"
#include "Box.h"
#include "LPFactory.h"

/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{


/*! 
 * \class	Bernstein
 * \brief	Bernstein class contains all the necessary code to generate Bernstein inequalities for multivariate monomials.
 *
 * The Bernstein class is responsible for generating Bernstein polytopes for multivariate monomials on a given box.
 * For the moment, it considers multivariate monomials up to degree 3, so x, x^2, x*y, x^3, x*y^2, x*y*z.
 * 
 */
template<typename LP_PROBLEM, typename SCALAR>
class Bernstein
{

public:
	/*!
     * \brief Default constructor. Builds an empty Bernstein object.
	 *
     * This constructor creates an empty Bernstein object.
	 * It is not used in the core program.
	 * It has to be parametered before using it.
	 * 
     */
	Bernstein();

	/*!
     * \brief Constructor from LPFactory
     * 
	 * This constructor creates a Bernstein object initialized with LPFactory.
	 *
	 * \param argp_lpf			: Pointer to the LPFactory to link to this Bernstein object.
	 *
     */
	Bernstein(LPFactory<LP_PROBLEM, SCALAR>* argp_lpf);

	/*!
     * \brief Destructor
	 *
     * The desructor must not delete its members because they point to the LPFactory members.
	 * LPFactory still requires its members, even after Bernstein is deleted.
	 *
     */
	~Bernstein();

public:

	/*!
     * \brief Pointer to the LPFactory.
	 *
     */
	LPFactory<LP_PROBLEM, SCALAR>* lpf;

public:
	/*!
     * \brief Build constraints for a given multivariate monomial
     *
     * Responsible for building all the Bernstein constraints for a given multivariate monomial, on a given box.
     *
     * \param argp_mm			: Pointer to multivariate monomial
	 * \param argp_box			: Pointer to box
	 *
	 * \return					: Vector of constraints
	 *
     */
	std::vector<Constraint<SCALAR>*> buildConstraints(Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_box);

	/*!
     * \brief Build constraints for all multivariate monomials in a given list
     *
     * Responsible for building all the Bernstein constraints for all multivariate monomials in a given list, on a given box.
     *
     * \param argp_mmList		: Pointer to list of monomials
	 * \param argp_box			: Pointer to box
	 *
	 * \return					: Vector of constraints
	 *
     */
	std::vector<Constraint<SCALAR>*> buildConstraints(std::map<int, Multivariate_Monomial<SCALAR>*>* argp_mmList, Box<SCALAR>* argp_box);

	/*!
     * \brief Build constraints for the monomial x^2
     *
     * Responsible for building all the Bernstein constraints for the monomial x^2, on a given box.
     *
     * \param argp_mm			: Pointer to the monomial x^2
	 * \param argp_box			: Pointer to box
	 *
	 * \return					: Vector of constraints
	 *
     */
	std::vector<Constraint<SCALAR>*> buildSquareConstraints(Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_box);

	/*!
     * \brief Build constraints for the monomial x*y
     *
     * Responsible for building all the Bernstein constraints for the monomial x*y, on a given box.
     *
     * \param argp_mm			: Pointer to the monomial x*y
	 * \param argp_box			: Pointer to box
	 *
	 * \return					: Vector of constraints
	 *
     */
	std::vector<Constraint<SCALAR>*> buildMixedConstraints(Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_box);

	/*!
     * \brief Build constraints for the monomial x^3
     *
     * Responsible for building all the Bernstein constraints for the monomial x^3, on a given box.
     *
     * \param argp_mm			: Pointer to the monomial x^3
	 * \param argp_box			: Pointer to box
	 *
	 * \return					: Vector of constraints
	 *
     */
	std::vector<Constraint<SCALAR>*> buildDegree3Variable1Constraints(Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_box);

	/*!
     * \brief Build constraints for the monomial x*y^2
     *
     * Responsible for building all the Bernstein constraints for the monomial x*y^2, on a given box.
     *
     * \param argp_mm			: Pointer to the monomial x*y^2
	 * \param argp_box			: Pointer to box
	 *
	 * \return					: Vector of constraints
	 *
     */
	std::vector<Constraint<SCALAR>*> buildDegree3Variable2Constraints(Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_box);

	/*!
     * \brief Build constraints for the multivariate monomial x*y*z
     *
     * Responsible for building all the Bernstein constraints for the multivariate monomial x*y*z, on a given box.
     *
     * \param argp_mm			: Pointer to the multivariate monomial x*y*z
	 * \param argp_box			: Pointer to box
	 *
	 * \return					: Vector of constraints
	 *
     */
	std::vector<Constraint<SCALAR>*> buildDegree3Variable3Constraints(Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_box);

}; // class Bernstein


// ############################ //


//
template<typename LP_PROBLEM, typename SCALAR>
Bernstein<LP_PROBLEM, SCALAR>::Bernstein()
{
}


//
template<typename LP_PROBLEM, typename SCALAR>
Bernstein<LP_PROBLEM, SCALAR>::Bernstein(LPFactory<LP_PROBLEM, SCALAR>* argp_lpf)
{
	this->lpf	= argp_lpf;
}


//
template<typename LP_PROBLEM, typename SCALAR>
Bernstein<LP_PROBLEM, SCALAR>::~Bernstein()
{
}


//
template<typename LP_PROBLEM, typename SCALAR>
std::vector<Constraint<SCALAR>*> Bernstein<LP_PROBLEM, SCALAR>::buildConstraints(Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_box)
{
	// TODO fabriquer les contraintes...
	std::vector<Constraint<SCALAR>*> res;

	// degree 1 and only 1 variable, so we have a simple monomial (i.e. a simple variable)
	if(argp_mm->degree == 1 && argp_mm->factors_list.size() == 1)
	{
		// do nothing
	}

	// degree 2 and only one variable, so it is square variable
	else if(argp_mm->degree == 2 && argp_mm->factors_list.size() == 1)
	{
		std::vector<Constraint<SCALAR>*> res_tmp	= this->buildSquareConstraints(argp_mm, argp_box);
		res.insert(res.end(), res_tmp.begin(), res_tmp.end());
	}

	// degree 2 and exactly two variables, so it is mixed variable
	else if(argp_mm->degree == 2 && argp_mm->factors_list.size() == 2)
	{
		std::vector<Constraint<SCALAR>*> res_tmp	= this->buildMixedConstraints(argp_mm, argp_box);
		res.insert(res.end(), res_tmp.begin(), res_tmp.end());
	}

	// degree 3 and exactly one variable, i.e. x^3
	else if(argp_mm->degree == 3 && argp_mm->factors_list.size() == 1)
	{
		std::vector<Constraint<SCALAR>*> res_tmp	= this->buildDegree3Variable1Constraints(argp_mm, argp_box);
		res.insert(res.end(), res_tmp.begin(), res_tmp.end());
	}

	// degree 3 and exactly two variables, i.e. x*y^2
	else if(argp_mm->degree == 3 && argp_mm->factors_list.size() == 2)
	{
		std::vector<Constraint<SCALAR>*> res_tmp	= this->buildDegree3Variable2Constraints(argp_mm, argp_box);
		res.insert(res.end(), res_tmp.begin(), res_tmp.end());
	}

	// degree 3 and exactly three variables, i.e. x*y*z
	else if(argp_mm->degree == 3 && argp_mm->factors_list.size() == 3)
	{
		std::vector<Constraint<SCALAR>*> res_tmp	= this->buildDegree3Variable3Constraints(argp_mm, argp_box);
		res.insert(res.end(), res_tmp.begin(), res_tmp.end());
	}

	// whatever else...
	else
	{
		// degree non managed
		std::cout << "Bernstein constraints not managed for this monomial..." << endl;
	}

	return res;
}


// build Bernstein constraints for all monomials in a given system for a given box
template<typename LP_PROBLEM, typename SCALAR>
std::vector<Constraint<SCALAR>*> Bernstein<LP_PROBLEM, SCALAR>::buildConstraints(std::map<int, Multivariate_Monomial<SCALAR>*>* argp_mmList, Box<SCALAR>* argp_box)
{
	// clear the former bernsteins
	std::vector<Constraint<SCALAR>*> res;

	// for each monomial of the system
	std::map<int, Multivariate_Monomial<SCALAR>*>::iterator it_mm;
	for(it_mm = argp_mmList->begin(); it_mm != argp_mmList->end(); it_mm++)
	{
		// build constraints for the current monomial
		std::vector<Constraint<SCALAR>*> tmp	= this->buildConstraints(it_mm->second, argp_box);

		// insert the constraints in the result
		res.insert(res.end(), tmp.begin(), tmp.end());
	}

	// return the list of all the system constraints
	return res;
}


// 
template<typename LP_PROBLEM, typename SCALAR>
std::vector<Constraint<SCALAR>*> Bernstein<LP_PROBLEM, SCALAR>::buildSquareConstraints(Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_box)
{
	std::vector<Constraint<SCALAR>*> res;

	// varexp has only one element in this case
	std::string mm_x_name				= std::get<0>(*(argp_mm->factors_list.begin()));
	int mm_x_id							= this->lpf->mmToC[mm_x_name];

	Multivariate_Monomial<SCALAR>* mm_x	= this->lpf->mm_list[mm_x_id];

	// bounds of the variable
	SCALAR u_tmp = argp_box->lower[mm_x_id];
	SCALAR v_tmp = argp_box->upper[mm_x_id];

	// we build the first constraint
	// (v - x) ^ 2 >= 0
	std::string name1		= "c1_" + argp_mm->name;
	Constraint<SCALAR>* c1	= new Constraint<SCALAR>(name1 , CONSTRAINT_LESS_THAN, v_tmp * v_tmp);
	c1->leftSide.addMonomial((SCALAR)-1, argp_mm);
	c1->leftSide.addMonomial((SCALAR)2 * v_tmp, mm_x);
	res.push_back(c1);

	// we build the third constraint
	// 2 * (x - u) * (v - x) >= 0
	std::string name2		= "c2_" + argp_mm->name;
	Constraint<SCALAR>* c2	= new Constraint<SCALAR>(name2 , CONSTRAINT_LESS_THAN, (SCALAR)-2 * u_tmp * v_tmp);
	c2->leftSide.addMonomial((SCALAR)2, argp_mm);
	c2->leftSide.addMonomial((SCALAR)-2 * (u_tmp + v_tmp), mm_x);
	res.push_back(c2);

	// we build the second constraint
	// (x - u) ^ 2 >= 0
	std::string name3		= "c3_" + argp_mm->name;
	Constraint<SCALAR>* c3	= new Constraint<SCALAR>(name3 , CONSTRAINT_LESS_THAN, u_tmp * u_tmp);
	c3->leftSide.addMonomial((SCALAR)-1, argp_mm);
	c3->leftSide.addMonomial((SCALAR)2 * u_tmp, mm_x);
	res.push_back(c3);

	// add other constraint
	// (x - 1/2) ^ 2 >= 0
	std::string name4		= "extra1_" + argp_mm->name;
	Constraint<SCALAR>* c4	= new Constraint<SCALAR>(name4 , CONSTRAINT_LESS_THAN, (SCALAR)0.25);
	c4->leftSide.addMonomial((SCALAR)-1, argp_mm);
	c4->leftSide.addMonomial((SCALAR)1, mm_x);
	res.push_back(c4);

	return res;

}
//*/


//
template<typename LP_PROBLEM, typename SCALAR>
std::vector<Constraint<SCALAR>*> Bernstein<LP_PROBLEM, SCALAR>::buildMixedConstraints(Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_box)
{
	std::vector<Constraint<SCALAR>*> res;

	// get the names of the two variables composing the monomial
	std::string var1_Name	= std::get<0>(*(argp_mm->factors_list.begin()));
	std::string var2_Name	= std::get<0>(*(argp_mm->factors_list.rbegin()));

	int var1_id				= this->lpf->mmToC[var1_Name];
	int var2_id				= this->lpf->mmToC[var2_Name];

	// get the submonomials, which are in reality variables represented as "simple" monomials
	Multivariate_Monomial<SCALAR>* mm_x	= this->lpf->mm_list[var1_id];
	Multivariate_Monomial<SCALAR>* mm_y	= this->lpf->mm_list[var2_id];

	// get the bounds of the two variables
	SCALAR u1_tmp = argp_box->lower[var1_id];
	SCALAR v1_tmp = argp_box->upper[var1_id];

	SCALAR u2_tmp = argp_box->lower[var2_id];
	SCALAR v2_tmp = argp_box->upper[var2_id];

	std::string name1		= "c1_" + argp_mm->name;
	Constraint<SCALAR>* c1	= new Constraint<SCALAR>(name1 , CONSTRAINT_LESS_THAN, v1_tmp * v2_tmp);
	c1->leftSide.addMonomial(-1, argp_mm);
	c1->leftSide.addMonomial(v2_tmp, mm_x);
	c1->leftSide.addMonomial(v1_tmp, mm_y);
	res.push_back(c1);

	std::string name2		= "c2_" + argp_mm->name;
	Constraint<SCALAR>* c2	= new Constraint<SCALAR>(name2 , CONSTRAINT_LESS_THAN, - v1_tmp * u2_tmp);
	c2->leftSide.addMonomial(1, argp_mm);
	c2->leftSide.addMonomial(- u2_tmp, mm_x);
	c2->leftSide.addMonomial(- v1_tmp, mm_y);
	res.push_back(c2);

	std::string name3		= "c3_" + argp_mm->name;
	Constraint<SCALAR>* c3	= new Constraint<SCALAR>(name3 , CONSTRAINT_LESS_THAN, - u1_tmp * v2_tmp);
	c3->leftSide.addMonomial(1, argp_mm);
	c3->leftSide.addMonomial(- v2_tmp, mm_x);
	c3->leftSide.addMonomial(- u1_tmp, mm_y);
	res.push_back(c3);
			
	std::string name4		= "c4_" + argp_mm->name;
	Constraint<SCALAR>* c4	= new Constraint<SCALAR>(name4 , CONSTRAINT_LESS_THAN, u1_tmp * u2_tmp);
	c4->leftSide.addMonomial(-1, argp_mm);
	c4->leftSide.addMonomial(u2_tmp, mm_x);
	c4->leftSide.addMonomial(u1_tmp, mm_y);
	res.push_back(c4);

	return res;
}
//*/


// 
template<typename LP_PROBLEM, typename SCALAR>
std::vector<Constraint<SCALAR>*> Bernstein<LP_PROBLEM, SCALAR>::buildDegree3Variable1Constraints(Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_box)
{
	std::vector<Constraint<SCALAR>*> res;
	
	// get the name of the variable, there is only one here, i.e. x^3
	std::string var1_Name	= std::get<0>(*(argp_mm->factors_list.begin()));

	// get the ids of the different monomials x, x^2 and x^3
	int var1_id				= this->lpf->mmToC[var1_Name];
	int var2_id				= this->lpf->mmToC[var1_Name + "^2"];
	int var3_id				= this->lpf->mmToC[var1_Name + "^3"];

	// get pointers to the submonomials corresponding to these ids...
	Multivariate_Monomial<SCALAR>* mm_x	= this->lpf->mm_list[var1_id];
	Multivariate_Monomial<SCALAR>* mm_y	= this->lpf->mm_list[var2_id];
	Multivariate_Monomial<SCALAR>* mm_z	= this->lpf->mm_list[var3_id];

	// get the bounds of the unique factor variable x
	SCALAR u_tmp = (this->lpf->mm_bounds[var1_id]).first;
	SCALAR v_tmp = (this->lpf->mm_bounds[var1_id]).second;

	std::string name1		= "c1_" + argp_mm->name;
	Constraint<SCALAR>* c1	= new Constraint<SCALAR>(name1 , CONSTRAINT_LESS_THAN, v_tmp * v_tmp * v_tmp);
	c1->leftSide.addMonomial((SCALAR)-1, mm_z);
	c1->leftSide.addMonomial((SCALAR)3 * v_tmp, mm_y);
	c1->leftSide.addMonomial((SCALAR)-3 * v_tmp * v_tmp, mm_x);
	res.push_back(c1);

	std::string name2		= "c2_" + argp_mm->name;
	Constraint<SCALAR>* c2	= new Constraint<SCALAR>(name2 , CONSTRAINT_LESS_THAN, - u_tmp * v_tmp * v_tmp);
	c2->leftSide.addMonomial(-1, mm_z);
	c2->leftSide.addMonomial(u_tmp + (SCALAR)2 * v_tmp, mm_y);
	c2->leftSide.addMonomial(- (SCALAR)2 * u_tmp * v_tmp - v_tmp * v_tmp, mm_x);
	res.push_back(c2);

	std::string name3		= "c3_" + argp_mm->name;
	Constraint<SCALAR>* c3	= new Constraint<SCALAR>(name3 , CONSTRAINT_LESS_THAN, u_tmp * u_tmp * v_tmp);
	c3->leftSide.addMonomial((SCALAR)1, mm_z);
	c3->leftSide.addMonomial((SCALAR)-2 * u_tmp - v_tmp, mm_y);
	c3->leftSide.addMonomial(u_tmp * u_tmp + (SCALAR)2 * u_tmp * v_tmp, mm_x);
	res.push_back(c3);
			
	std::string name4		= "c4_" + argp_mm->name;
	Constraint<SCALAR>* c4	= new Constraint<SCALAR>(name4 , CONSTRAINT_LESS_THAN, - u_tmp * u_tmp * u_tmp);
	c4->leftSide.addMonomial((SCALAR)-1, mm_z);
	c4->leftSide.addMonomial((SCALAR)3 * u_tmp, mm_y);
	c4->leftSide.addMonomial((SCALAR)-3 * u_tmp * u_tmp, mm_x);
	res.push_back(c4);

	return res;
}
//


// we build the constraints for xy^2, so the factor with degree 2 is considered as y, and the other as x
template<typename LP_PROBLEM, typename SCALAR>
std::vector<Constraint<SCALAR>*> Bernstein<LP_PROBLEM, SCALAR>::buildDegree3Variable2Constraints(Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_box)
{
	std::vector<Constraint<SCALAR>*> res;
	
	// iterator on factors list
	std::set<std::tuple<std::string, int>>::iterator it_factor	= argp_mm->factors_list.begin();

	// get the names such as var_y is the degree 2 variable
	std::string var_x, var_y;

	// if degree of first factor is 2, then var_y is the first factor
	if(std::get<1>(*it_factor) == 2)
	{
		var_y	= std::get<0>(*it_factor);
		it_factor++;
		var_x	= std::get<0>(*it_factor);
	}
	// or else, var1Id is the second factor of the list
	else
	{
		var_x	= std::get<0>(*it_factor);
		it_factor++;
		var_y	= std::get<0>(*it_factor);
	}

	// TODO be careful with the order of factors to get the monomial names!
	// get the ids of the different monomials x, x^2 and x^3
	int var_x_id				= this->lpf->mmToC[var_x];
	int var_y_id				= this->lpf->mmToC[var_y];
	// problem with the names
	//int mm_xy2_id				= this->lpf->mmToC->operator[](var_x + "*" + var_y + "^2");
	int mm_xy2_id				= this->lpf->mmToC[argp_mm->name];
	int mm_y2_id				= this->lpf->mmToC[var_y + "^2"];

	// get xy id depending on xy name, if name of x < name of y so its xy, or else it is yx
	int mm_xy_id				= (var_x < var_y) ? this->lpf->mmToC[var_x + "*" + var_y] : this->lpf->mmToC[var_y + "*" + var_x];

	// get pointers to the submonomials corresponding to these ids...
	Multivariate_Monomial<SCALAR>* mm_x		= this->lpf->mm_list[var_x_id];
	Multivariate_Monomial<SCALAR>* mm_y		= this->lpf->mm_list[var_y_id];
	Multivariate_Monomial<SCALAR>* mm_xy2	= this->lpf->mm_list[mm_xy2_id];
	Multivariate_Monomial<SCALAR>* mm_y2	= this->lpf->mm_list[mm_y2_id];
	Multivariate_Monomial<SCALAR>* mm_xy	= this->lpf->mm_list[mm_xy_id];

	// get the bounds of the variable / monomial x
	SCALAR ux_tmp	= (this->lpf->mm_bounds[var_x_id]).first;
	SCALAR vx_tmp	= (this->lpf->mm_bounds[var_x_id]).second;

	// get the bounds of the variable / monomial y
	SCALAR uy_tmp	= (this->lpf->mm_bounds[var_y_id]).first;
	SCALAR vy_tmp	= (this->lpf->mm_bounds[var_y_id]).second;
	
	std::string name1		= "c1_" + argp_mm->name;
	Constraint<SCALAR>* c1	= new Constraint<SCALAR>(name1 , CONSTRAINT_LESS_THAN, vx_tmp * vy_tmp * vy_tmp);
	c1->leftSide.addMonomial(vy_tmp * vy_tmp, mm_x);
	c1->leftSide.addMonomial((SCALAR)2 * vx_tmp * vy_tmp, mm_y);
	c1->leftSide.addMonomial((SCALAR)-1 * vx_tmp, mm_y2);
	c1->leftSide.addMonomial((SCALAR)-2 * vy_tmp, mm_xy);
	c1->leftSide.addMonomial((SCALAR)1, mm_xy2);
	res.push_back(c1);
	
	std::string name2		= "c2_" + argp_mm->name;
	Constraint<SCALAR>* c2	= new Constraint<SCALAR>(name2 , CONSTRAINT_LESS_THAN, (SCALAR)-1 * vx_tmp * uy_tmp * vy_tmp);
	c2->leftSide.addMonomial((SCALAR)-1 * uy_tmp * vy_tmp, mm_x);
	c2->leftSide.addMonomial((SCALAR)-1 * vx_tmp * (uy_tmp + vy_tmp), mm_y);
	c2->leftSide.addMonomial((SCALAR)-1 * vx_tmp, mm_y2);
	c2->leftSide.addMonomial(uy_tmp + vy_tmp, mm_xy);
	c2->leftSide.addMonomial((SCALAR)1, mm_xy2);
	res.push_back(c2);
	
	std::string name3		= "c3_" + argp_mm->name;
	Constraint<SCALAR>* c3	= new Constraint<SCALAR>(name3 , CONSTRAINT_LESS_THAN, uy_tmp * uy_tmp * vx_tmp);
	c3->leftSide.addMonomial(uy_tmp * uy_tmp, mm_x);
	c3->leftSide.addMonomial((SCALAR)2 * uy_tmp * vx_tmp, mm_y);
	c3->leftSide.addMonomial((SCALAR)-1 * vx_tmp, mm_y2);
	c3->leftSide.addMonomial((SCALAR)-2 * uy_tmp, mm_xy);
	c3->leftSide.addMonomial((SCALAR)1, mm_xy2);
	res.push_back(c3);
	
	std::string name4		= "c4_" + argp_mm->name;
	Constraint<SCALAR>* c4	= new Constraint<SCALAR>(name4 , CONSTRAINT_LESS_THAN, (SCALAR)-1 * ux_tmp * vy_tmp * vy_tmp);
	c4->leftSide.addMonomial((SCALAR)-1 * vy_tmp * vy_tmp, mm_x);
	c4->leftSide.addMonomial((SCALAR)-2 * ux_tmp * vy_tmp, mm_y);
	c4->leftSide.addMonomial(ux_tmp, mm_y2);
	c4->leftSide.addMonomial((SCALAR)2 * vy_tmp, mm_xy);
	c4->leftSide.addMonomial((SCALAR)-1, mm_xy2);
	res.push_back(c4);

	std::string name5		= "c5_" + argp_mm->name;
	Constraint<SCALAR>* c5	= new Constraint<SCALAR>(name5 , CONSTRAINT_LESS_THAN, ux_tmp * uy_tmp * vy_tmp);
	c5->leftSide.addMonomial(uy_tmp * vy_tmp, mm_x);
	c5->leftSide.addMonomial(ux_tmp * (uy_tmp + vy_tmp), mm_y);
	c5->leftSide.addMonomial((SCALAR)-1 * ux_tmp, mm_y2);
	c5->leftSide.addMonomial((SCALAR)-1 * (uy_tmp + vy_tmp), mm_xy);
	c5->leftSide.addMonomial((SCALAR)1, mm_xy2);
	res.push_back(c5);

	std::string name6		= "c6_" + argp_mm->name;
	Constraint<SCALAR>* c6	= new Constraint<SCALAR>(name6 , CONSTRAINT_LESS_THAN, (SCALAR)-1 * ux_tmp * uy_tmp * uy_tmp);
	c6->leftSide.addMonomial((SCALAR)-1 * uy_tmp * uy_tmp, mm_x);
	c6->leftSide.addMonomial((SCALAR)-2 * ux_tmp * uy_tmp, mm_y);
	c6->leftSide.addMonomial(ux_tmp, mm_y2);
	c6->leftSide.addMonomial((SCALAR)2 * uy_tmp, mm_xy);
	c6->leftSide.addMonomial((SCALAR)-1, mm_xy2);
	res.push_back(c6);
	
	/*
	cout << *c1 << endl;
	cout << *c2 << endl;
	cout << *c3 << endl;
	cout << *c4 << endl;
	cout << *c5 << endl;
	cout << *c6 << endl;
	*/

	return res;
}
//


// 
template<typename LP_PROBLEM, typename SCALAR>
std::vector<Constraint<SCALAR>*> Bernstein<LP_PROBLEM, SCALAR>::buildDegree3Variable3Constraints(Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_box)
{
	
	std::vector<Constraint<SCALAR>*> res;
	
	// iterator on factors list
	std::set<std::tuple<std::string, int>>::iterator it_factor	= argp_mm->factors_list.begin();

	// get the names such as var_x is the degree 2 variable
	std::string var_x, var_y, var_z;

	var_x	= std::get<0>(*it_factor);
	it_factor++;
	var_y	= std::get<0>(*it_factor);
	it_factor++;
	var_z	= std::get<0>(*it_factor);
	
	// get the ids of variables x, y and z
	int mm_x_id	= this->lpf->mmToC[var_x];
	int mm_y_id	= this->lpf->mmToC[var_y];
	int mm_z_id	= this->lpf->mmToC[var_z];

	// TODO be aware of the factors name! monomial yx is called xy!
	// get the ids of the different monomials x*y, x*z and y*z
	int mm_xy_id	= (var_x < var_y) ? this->lpf->mmToC[var_x + "*" + var_y] : this->lpf->mmToC[var_y + "*" + var_x];
	int mm_xz_id	= (var_x < var_z) ? this->lpf->mmToC[var_x + "*" + var_z] : this->lpf->mmToC[var_z + "*" + var_x];
	int mm_yz_id	= (var_y < var_z) ? this->lpf->mmToC[var_y + "*" + var_z] : this->lpf->mmToC[var_z + "*" + var_y];
	
	int mm_xyz_id	= this->lpf->mmToC[argp_mm->name];

	// get pointers to simple monomials, i.e. variables
	Multivariate_Monomial<SCALAR>* mm_x	= this->lpf->mm_list[mm_x_id];
	Multivariate_Monomial<SCALAR>* mm_y	= this->lpf->mm_list[mm_y_id];
	Multivariate_Monomial<SCALAR>* mm_z	= this->lpf->mm_list[mm_z_id];

	// get pointers to the submonomials corresponding to these ids...
	Multivariate_Monomial<SCALAR>* mm_xy	= this->lpf->mm_list[mm_xy_id];
	Multivariate_Monomial<SCALAR>* mm_xz	= this->lpf->mm_list[mm_xz_id];
	Multivariate_Monomial<SCALAR>* mm_yz	= this->lpf->mm_list[mm_yz_id];
	
	Multivariate_Monomial<SCALAR>* mm_xyz	= this->lpf->mm_list[mm_xyz_id];

	// get the bounds of x
	SCALAR u_x	= (this->lpf->mm_bounds[mm_x_id]).first;
	SCALAR v_x	= (this->lpf->mm_bounds[mm_x_id]).second;

	// get the bounds of y
	SCALAR u_y	= (this->lpf->mm_bounds[mm_y_id]).first;
	SCALAR v_y	= (this->lpf->mm_bounds[mm_y_id]).second;

	// get the bounds of z
	SCALAR u_z	= (this->lpf->mm_bounds[mm_z_id]).first;
	SCALAR v_z	= (this->lpf->mm_bounds[mm_z_id]).second;

	std::string name1		= "c1_" + argp_mm->name;
	Constraint<SCALAR>* c1	= new Constraint<SCALAR>(name1 , CONSTRAINT_LESS_THAN, v_x * v_y * v_z);
	c1->leftSide.addMonomial((SCALAR)-1, mm_xyz);
	c1->leftSide.addMonomial((SCALAR)-1 * v_z, mm_xy);
	c1->leftSide.addMonomial((SCALAR)-1 * v_y, mm_xz);
	c1->leftSide.addMonomial((SCALAR)-1 * v_x, mm_yz);
	c1->leftSide.addMonomial(v_y * v_z, mm_x);
	c1->leftSide.addMonomial(v_x * v_z, mm_y);
	c1->leftSide.addMonomial(v_x * v_y, mm_z);
	res.push_back(c1);

	std::string name2		= "c2_" + argp_mm->name;
	Constraint<SCALAR>* c2	= new Constraint<SCALAR>(name2 , CONSTRAINT_LESS_THAN, (SCALAR)-1 * v_x * v_y * u_z);
	c2->leftSide.addMonomial((SCALAR)1, mm_xyz);
	c2->leftSide.addMonomial(u_z, mm_xy);
	c2->leftSide.addMonomial(v_y, mm_xz);
	c2->leftSide.addMonomial(v_x, mm_yz);
	c2->leftSide.addMonomial((SCALAR)-1 * v_y * u_z, mm_x);
	c2->leftSide.addMonomial((SCALAR)-1 * v_x * u_z, mm_y);
	c2->leftSide.addMonomial((SCALAR)-1 * v_x * v_y, mm_z);
	res.push_back(c2);

	std::string name3		= "c3_" + argp_mm->name;
	Constraint<SCALAR>* c3	= new Constraint<SCALAR>(name3 , CONSTRAINT_LESS_THAN, (SCALAR)-1 * v_x * u_y * v_z);
	c3->leftSide.addMonomial((SCALAR)1, mm_xyz);
	c3->leftSide.addMonomial(v_z, mm_xy);
	c3->leftSide.addMonomial(u_y, mm_xz);
	c3->leftSide.addMonomial(v_x, mm_yz);
	c3->leftSide.addMonomial((SCALAR)-1 * u_y * v_z, mm_x);
	c3->leftSide.addMonomial((SCALAR)-1 * v_x * v_z, mm_y);
	c3->leftSide.addMonomial((SCALAR)-1 * v_x * u_y, mm_z);
	res.push_back(c3);
			
	std::string name4		= "c4_" + argp_mm->name;
	Constraint<SCALAR>* c4	= new Constraint<SCALAR>(name4 , CONSTRAINT_LESS_THAN, v_x * u_y * u_z);
	c4->leftSide.addMonomial((SCALAR)-1, mm_xyz);
	c4->leftSide.addMonomial((SCALAR)-1 * u_z, mm_xy);
	c4->leftSide.addMonomial((SCALAR)-1 * u_y, mm_xz);
	c4->leftSide.addMonomial((SCALAR)-1 * v_x, mm_yz);
	c4->leftSide.addMonomial(u_y * u_z, mm_x);
	c4->leftSide.addMonomial(v_x * u_z, mm_y);
	c4->leftSide.addMonomial(v_x * u_y, mm_z);
	res.push_back(c4);

	std::string name5		= "c5_" + argp_mm->name;
	Constraint<SCALAR>* c5	= new Constraint<SCALAR>(name5 , CONSTRAINT_LESS_THAN, (SCALAR)-1 * u_x * v_y * v_z);
	c5->leftSide.addMonomial((SCALAR)1, mm_xyz);
	c5->leftSide.addMonomial(v_z, mm_xy);
	c5->leftSide.addMonomial(v_y, mm_xz);
	c5->leftSide.addMonomial(u_x, mm_yz);
	c5->leftSide.addMonomial((SCALAR)-1 * v_y * v_z, mm_x);
	c5->leftSide.addMonomial((SCALAR)-1 * u_x * v_z, mm_y);
	c5->leftSide.addMonomial((SCALAR)-1 * u_x * v_y, mm_z);
	res.push_back(c5);

	std::string name6		= "c6_" + argp_mm->name;
	Constraint<SCALAR>* c6	= new Constraint<SCALAR>(name6 , CONSTRAINT_LESS_THAN, u_x * v_y * u_z);
	c6->leftSide.addMonomial((SCALAR)-1, mm_xyz);
	c6->leftSide.addMonomial((SCALAR)-1 * u_z, mm_xy);
	c6->leftSide.addMonomial((SCALAR)-1 * v_y, mm_xz);
	c6->leftSide.addMonomial((SCALAR)-1 * u_x, mm_yz);
	c6->leftSide.addMonomial(v_y * u_z, mm_x);
	c6->leftSide.addMonomial(u_x * u_z, mm_y);
	c6->leftSide.addMonomial(u_x * v_y, mm_z);
	res.push_back(c6);

	std::string name7		= "c7_" + argp_mm->name;
	Constraint<SCALAR>* c7	= new Constraint<SCALAR>(name7 , CONSTRAINT_LESS_THAN, u_x * u_y * v_z);
	c7->leftSide.addMonomial((SCALAR)-1, mm_xyz);
	c7->leftSide.addMonomial((SCALAR)-1 * v_z, mm_xy);
	c7->leftSide.addMonomial((SCALAR)-1 * u_y, mm_xz);
	c7->leftSide.addMonomial((SCALAR)-1 * u_x, mm_yz);
	c7->leftSide.addMonomial(u_y * v_z, mm_x);
	c7->leftSide.addMonomial(u_x * v_z, mm_y);
	c7->leftSide.addMonomial(u_x * u_y, mm_z);
	res.push_back(c7);

	std::string name8		= "c8_" + argp_mm->name;
	Constraint<SCALAR>* c8	= new Constraint<SCALAR>(name8 , CONSTRAINT_LESS_THAN, (SCALAR)-1 * u_x * u_y * u_z);
	c8->leftSide.addMonomial((SCALAR)1, mm_xyz);
	c8->leftSide.addMonomial(u_z, mm_xy);
	c8->leftSide.addMonomial(u_y, mm_xz);
	c8->leftSide.addMonomial(u_x, mm_yz);
	c8->leftSide.addMonomial((SCALAR)-1 * u_y * u_z, mm_x);
	c8->leftSide.addMonomial((SCALAR)-1 * u_x * u_z, mm_y);
	c8->leftSide.addMonomial((SCALAR)-1 * u_x * u_y, mm_z);
	res.push_back(c8);
	
	return res;
	
}
//


} // namespace bbs


#endif