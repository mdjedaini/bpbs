#ifndef _SHRINKER_DEFAULT_H_
#define _SHRINKER_DEFAULT_H_

//#include "LPFactory.h"
#include "System.h"
#include "Box.h"

namespace bbs
{


template<typename SCALAR, typename LP_FACTORY>
class Shrinker_Default
{

public:
	Shrinker_Default(void);
	~Shrinker_Default(void);

public:
	int operator()(System<SCALAR>* argp_sm, Box<SCALAR>* argp_b, LP_FACTORY* argp_lpf);

};


// ###################### //


//
template<typename SCALAR, typename LP_FACTORY>
Shrinker_Default<SCALAR, LP_FACTORY>::Shrinker_Default(void)
{
}


//
template<typename SCALAR, typename LP_FACTORY>
Shrinker_Default<SCALAR, LP_FACTORY>::~Shrinker_Default(void)
{
}


//
template<typename SCALAR, typename LP_FACTORY>
int Shrinker_Default<SCALAR, LP_FACTORY>::operator()(System<SCALAR>* argp_s, Box<SCALAR>* argp_b, LP_FACTORY* argp_lpf)
{
	// we proceed like that to make sure each variable will have its stability updated
	// if not, variable which do not require shrinking will not have their stability updated
	std::vector<SCALAR> new_lower	= std::vector<SCALAR>(argp_b->lower);			// lower bounds of the box
	std::vector<SCALAR> new_upper	= std::vector<SCALAR>(argp_b->upper);			// upper bounds of the box

	// get variables to shrink
	std::vector<std::string> varToShrink	= argp_b->getVariablesToShrink();

	// create the lp model
	LP_FACTORY::internal_lp_problem* model	= argp_lpf->createLPModel(argp_s, argp_b);

	std::vector<std::string>::iterator it_vts;
	for(it_vts = varToShrink.begin(); it_vts != varToShrink.end(); it_vts++)
	{
		// create the objective function
		std::vector<int>	obj_id_list;
		std::vector<SCALAR>	obj_coeff_list;

		// the coeff is 1, the objective function is the current variable itself
		obj_id_list.push_back(argp_s->mmToC[*it_vts]);
		obj_coeff_list.push_back((SCALAR)1);

		// LP for minimizing the sum
		LP_FACTORY::internal_lp_problem* lp_min	= argp_lpf->buildLP(model);
		lp_min->setObjective(-1, obj_id_list, obj_coeff_list);

		// lp for maximizing the sum
		LP_FACTORY::internal_lp_problem* lp_max	= argp_lpf->buildLP(model);
		lp_max->setObjective(1, obj_id_list, obj_coeff_list);

		// solve the lp
		lp_min->solve();
		
		if(lp_min->getStatus() != 0)
		{
			return 0;
		}

		// solve the lp
		lp_max->solve();

		// get the variable id
		int varId	= argp_s->vToC[*it_vts];

		// update inf bound of the current variable
		new_lower[varId]	= lp_min->getOptimum();
		
		// update sup bound of the current variable
		new_upper[varId]	= lp_max->getOptimum();

		//cout << "Min " << *it_vts << ": " << lp_min->getOptimum() << endl;
		//cout << "Max " << *it_vts << ": " << lp_max->getOptimum() << endl;

		// delete the lps
		delete lp_min;
		delete lp_max;
	}

	// update all bounds in the box
	argp_b->updateBounds(new_lower, new_upper);

	// return 1 because shrink done successfully
	return 1;
}


} // namespace bbs


#endif