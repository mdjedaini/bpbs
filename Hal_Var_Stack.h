#ifndef _HAL_VAR_STACK_H_
#define _HAL_VAR_STACK_H_

#include "Box.h"

namespace bbs
{


template<typename SCALAR>
class Hal_Var_Stack
{

public:
	Hal_Var_Stack(void);
	~Hal_Var_Stack(void);

public:
	bool operator()(Box<SCALAR>* b);

};


// ########################### //


//
template<typename SCALAR>
Hal_Var_Stack<SCALAR>::Hal_Var_Stack(void)
{
}


//
template<typename SCALAR>
Hal_Var_Stack<SCALAR>::~Hal_Var_Stack(void)
{
}


// do we have to split, or not?
template<typename SCALAR>
bool Hal_Var_Stack<SCALAR>::operator()(Box<SCALAR>* argp_b)
{
	return true;
}


} // namespace bbs

#endif

