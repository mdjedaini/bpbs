// Simple Multivariate Polynomial Parser
// (c) George M. Tzoumas, LE2I, Université de Bourgogne, Dijon, France, 2012
//
// portions (c) Christoph Fuenfzig, LE2I, Université de Bourgogne, Dijon, France, 2009.

/*

Multivariate polynomials are considered to follow this grammar:

Trivial_monomial ::= Numeric_constant | Symbol | Symbol "^" Int_exponent
Monomial         ::= Trivial_monomial | Trivial_monomial "*" Monomial
Polynomial       ::= Monomial | Monomial "+" Polynomial

*/


#ifndef _MPOLY_PARSER_H
#define _MPOLY_PARSER_H

#include "symbolicc++.h"

#include <boost/algorithm/string.hpp>
#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_ast.hpp>

#include "spirit_evaluator.h"

/** State class for spirit parser. */
class state_t
{
public:
  state_t () : first(-2), second(0.0), third()
  {}
  virtual ~state_t ()
  {}

  const state_t& operator= (const state_t& b) {
     first  = b.first;
     second = b.second;
     third  = b.third;
     return *this;
  }
  /** State: -2 for expression, -1 for a constant, >=0 for variable ids. */
  int      first;
  /** Not used anymore. */
  double   second;
  /** Symbolic for parsed expression. */
  Symbolic third;
};


// <variable,exponent> pair
typedef std::pair<std::string, int> varexp_t;

// monomial representation
template<typename T>
struct monom_t
{
	T coeff;						// coefficient
	std::vector<varexp_t> var;		// list of variable^exponent
	int degree;						// monomial total degree
	monom_t(T c = T()): degree(0), coeff(c) { }
	// update list with a new variable and exponent (no check for existing vars)
	
	void add_variable(const std::string &name, int exp)
	{
		var.push_back(std::make_pair(name, exp));
		degree += exp;
	}
	
	// trivially multiply two monomials (assuming different vars
	// just merge lists and sum degree)
	void trivial_mul(const struct monom_t<T> &other)
	{
		coeff *= other.coeff;
		var.insert(var.end(), other.var.begin(), other.var.end());
		degree += other.degree;
	}
};


// ** AUXILIARY ROUTINES -- not to be called directly **

// get trivial monomial from a Symbolic object:
// trivial monomials are Numeric, Symbol and Power
template<typename T>
monom_t<T> get_trivial_monomial(const Symbolic &q)
{
	monom_t<T> mon;
	//    std::cerr << "get_trivial_monomial(): " << q.type().name() << std::endl;
    if (q.type() == typeid(Numeric)) { // e.g. poly = 5.24
        CastPtr<Numeric> xn(q);
        if (xn->numerictype() == typeid(double)) {
            CastPtr<Number<double> > x(q);
            mon.coeff = static_cast<T>(x->n);
        } else if (xn->numerictype() == typeid(int)) {
            CastPtr<Number<int> > x(q);
            mon.coeff = static_cast<T>(x->n);
        } else {
            std::cerr << "get_trivial_monomial(): unknown Numeric type" << std::endl;
        }
    } else if (q.type() == typeid(Symbol)) { // e.g. poly = x
        CastPtr<Symbol> x(q);
        mon.coeff = 1.0;
        mon.add_variable(x->name, 1);
    } else if (q.type() == typeid(Power)) { // e.g. poly = x^2
        CastPtr<Symbol> x(q);
        CastPtr<Symbol> v(x->parameters.front());
        CastPtr<Number<int> > e(x->parameters.back());
        mon.coeff = 1.0;
        mon.add_variable(v->name, e->n);
    } else {
        std::cerr << "get_trivial_monomial(): unknown Symbolic object of type " <<
                     q.type().name() << ":" << q << std::endl;
    }
//    std::cerr << "get_trivial_monomial(): exit" << std::endl;
    return mon;
}


// get a monomial from a Symbolic object,
// which is either a trivial monomial, or a Product of trivial monomials
template<typename T>
monom_t<T> get_monomial(const Symbolic &q)
{
	// std::cerr << "get_monomial(): " << q.type().name() << std::endl;
    if (q.type() == typeid(Numeric) || q.type() == typeid(Symbol) || q.type() == typeid(Power))
	{
		return get_trivial_monomial<T>(q);
	}
	else if (q.type() == typeid(Product))
	{
		// e.g. poly = 5*x*y
		monom_t<T> mon(1.0);
        CastPtr<Product> x(q);
        std::list<Symbolic>::iterator it;
        for ( it = x->factors.begin(); it != x->factors.end(); it++)
		{
			mon.trivial_mul(get_trivial_monomial<T>(*it));
		}
		return mon;
	}
    std::cerr << "get_monomial(): unknown Symbolic object of type " << q.type().name() << ":" << q << std::endl;
    return monom_t<T>();
}


// get a multivariate polynomial from a Symbolic object,
// which is either a monomial, or a Sum of monomials
template<typename T>
std::vector<monom_t<T> > get_monomials(const Symbolic &q)
{
	// std::cerr << "get_monomials(): " << q.type().name() << std::endl;
	std::vector<monom_t<T>> res;
	
	if (q.type() == typeid(Numeric) || q.type() == typeid(Symbol) || q.type() == typeid(Power))
	{
		res.push_back(get_trivial_monomial<T>(q));
	}
	
	else if (q.type() == typeid(Product))
	{
		res.push_back(get_monomial<T>(q));
	}
	
	else if (q.type() == typeid(Sum))
	{
		CastPtr<Sum> x(q);
		std::list<Symbolic>::iterator it;
		for ( it = x->summands.begin(); it != x->summands.end(); it++)
		{
			// std::cerr << *it << std::endl;
			res.push_back(get_monomial<T>(*it));
		}
	}
	
	else
	{
		std::cerr << "get_monomials(): unknown Symbolic object of type " << q.type().name() << ":" << q << std::endl;
	}

	return res;
}

// routine to expand powers of expressions
Symbolic expanded_power(Symbolic x, int k);

// Convert AST to LP row:
static state_t parse_subtree(const tree_iter_t& i)
{
  state_t result;
  
  if (i->value.id() == expression::factorID)
  { // constant
     // Simple alpha-numeric literal
     std::string ident = std::string(i->value.begin(), i->value.end());
     // CF unfortunately this is necessary to remove whitespaces
     boost::trim(ident);
     if (boost::algorithm::is_digit()(ident[0]) || ident[0] == '+' || ident[0] == '-')
	 {
       result.first  = -1; // constant
       result.second = atof(ident.c_str());
       result.third  = Symbolic(result.second);
     }
	 else
	 {
       result.third  = Symbolic(ident);
       result.second = 1.0;
       result.first  = 0; // variable
     }
     return result;
  }

  if (i->value.id() == expression::unaryID)
  { // unary expression
    result       = parse_subtree(i->children.begin());
    result.third = -result.third;
    return result;
  }

  // binary expressions
  state_t result1, result2;
  tree_iter_t j = i->children.begin();
  tree_iter_t k = i->children.begin()+1;
  std::string ident1 = std::string(j->value.begin(), j->value.end());
  std::string ident2 = std::string(k->value.begin(), k->value.end());
  // CF unfortunately this is necessary to remove whitespaces
  trim(ident1);
  trim(ident2);
  result1 = parse_subtree(j);
  result2 = parse_subtree(k);
  switch(*i->value.begin())
  {
  case '^':
      result.first = -2;
      result.third = expanded_power(result1.third, static_cast<int>(result2.second));
      result.first  = -2; // expression
      return result;
  case '*':
    result.first  = -2; // expression
    result.third  = result1.third *result2.third;
    return result;
  case '/':
    result.first  = -2; // expression
    result.third  = (result1.third)/(result2.third);
    return result;
  case '+':
     result.first  = -2; // expression
     result.third = result1.third + result2.third;
     return result;
  case '-':
     result.first  = -2; // expression
     result.third = result1.third - result2.third;
     return result;
  }
  std::cerr << "WARNING: node of unknown type!" << std::endl;
  return result;
}

// ** USER ROUTINES -- exported interface **

// return a Symbolic object from an input string (expression parsing)
// return value is true on successful parsing, false otherwise
bool mpoly_symbolic_parse (const std::string &input, Symbolic &s);

// return a list of monomials from an input string (expression & monomial parsing)
template<typename SCALAR>
std::vector<monom_t<SCALAR>> mpoly_parse (const std::string &input)
{
	//std::cout << "Expression a parser: " << input << endl;

	std::vector<monom_t<SCALAR>> res;
	Symbolic s;
	
	if (mpoly_symbolic_parse(input, s))
	{
		res = get_monomials<SCALAR>(s);
	}
	
	return res;
}

#endif // MPOLY_PARSER_H
