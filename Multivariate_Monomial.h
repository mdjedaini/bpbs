#ifndef _MULTIVARIATE_MONOMIAL_H_
#define _MULTIVARIATE_MONOMIAL_H_

/*!
 * \file		Multivariate_Monomial.h
 * \brief		File containing the Multivariate_Monomial class, responsible for representing internally multivariate monomials.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * File containing the Multivariate_Monomial class, responsible for representing internally multivariate monomials.
 *
 */


/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{

/*! 
 * \class	Multivariate_Monomial
 * \brief	Multivariate_Monomial internally figures out multivariate monomials.
 *
 * Multivariate_Monomial internally figures out multivariate monomials.
 * It contains a list of factors, the degree and a name for the monomial.
 * Monomial name is computed internally such as it is a bijection with the monomial space.
 */
template<typename SCALAR>
class Multivariate_Monomial
{

public:
	/*!
     * \brief Default constructor
     *
	 * Creates an empty Multivariate_Monomial object, and initialize the degree to 0 and the name to the null string.
     */
	Multivariate_Monomial();

	/*!
     * \brief Constructor from variable name and exponent.
     *
     * Builds a Multivariate_Monomial object from the provided variable name and exponent.
     * It is in fact an univariate monomial.
	 *
     * \param arg_varName		: Variable name
	 * \param arg_exp			: Exponent
	 *
     */
	Multivariate_Monomial(std::string arg_varName, int arg_exp);

	/*!
     * \brief Destructor
     *
     * Destructor does not need to destroy anything.
     *
     */
	~Multivariate_Monomial();

public:
	/*!
     * \brief List of variable names and their exponent in the monomial.
	 *
     */
	std::set<std::tuple<std::string, int>> factors_list;

	/*!
     * \brief Degree of the monomial, computed internally.
	 *
     */
	int degree;

	/*!
     * \brief Monomial name, computed internally such as it is a bijection with the monomial space.
	 *
     */
	std::string name;

	//SCALAR infBound;
	//SCALAR supBound;

public:
	/*!
     * \brief Adds a factor
     *
     * Adds a new factor to the current monomial
     *
     * \param arg_varName		: Variable name
	 * \param arg_varName		: Variable ID
	 * \param arg_exp			: Exponent
	 *
     */
	void addFactor(std::string arg_varName, int arg_exp);

	/*!
     * \brief Computes the name of the monomial
     *
     * Computes the name of the current monomial based on the factors in it.
	 * The same real monomials will have the same name, and two different monomial will have different names.
	 * This function deals with factors order.
	 *
     */
	void computeName();

public:
	/*!
     * \brief Computes the name of the monomial
     *
     * Computes the name of the current monomial based on the factors in it.
	 * The same real monomials will have the same name, and two different monomial will have different names.
	 * This function deals with factors order.
	 *
     */
	bool operator<(const Multivariate_Monomial<SCALAR>& other) const;

	/*!
     * \brief Computes the name of the monomial
     *
     * Computes the name of the current monomial based on the factors in it.
	 * The same real monomials will have the same name, and two different monomial will have different names.
	 * This function deals with factors order.
	 *
     */
	bool operator==(const Multivariate_Monomial<SCALAR>& other) const;

public:
	/*!
     * \brief Multivariate_Monomial output operator
     *
     * \param argr_o	: Output stream
	 * \param argr_mm	: Monomial to output
	 *
	 * \return			: Output stream
     */
	template<typename SCALAR>
	friend std::ostream& operator<< (std::ostream& argr_o, const Multivariate_Monomial<SCALAR>& argr_mm);

	/*!
     * \brief Multivariate_Monomial input operator
     *
     * \param argr_i	: Input stream
	 * \param argr_mm	: Monomial to fill in from input stream
	 *
	 * \return			: Input stream
     */
	template<typename SCALAR>
	friend std::istream& operator>> (std::istream& argr_i, Multivariate_Monomial<SCALAR>& argr_mm);

};


// ############# //


//
template<typename SCALAR>
Multivariate_Monomial<SCALAR>::Multivariate_Monomial()
{
	this->name		= "";
	this->degree	= 0;
}


// 
template<typename SCALAR>
Multivariate_Monomial<SCALAR>::Multivariate_Monomial(std::string arg_varName, int arg_exp)
{
	// add the factor to the factors list
	this->factors_list.insert(std::make_tuple(arg_varName, arg_exp));

	// compute the name of the monomial
	this->computeName();

	this->degree	= arg_exp;
}


//
template<typename SCALAR>
Multivariate_Monomial<SCALAR>::~Multivariate_Monomial()
{
}


// 
template<typename SCALAR>
void Multivariate_Monomial<SCALAR>::addFactor(std::string arg_varName, int arg_exp)
{
	// add the factor to the factors list
	this->factors_list.insert(std::make_tuple(arg_varName, arg_exp));
	
	// 
	this->degree += arg_exp;

	// 
	this->computeName();
}


// 
template<typename SCALAR>
void Multivariate_Monomial<SCALAR>::computeName()
{
	this->name	= "";

	std::set<std::tuple<std::string, int>>::iterator it_varExp;
	for(it_varExp = this->factors_list.begin(); it_varExp != this->factors_list.end(); it_varExp++)
	{
		this->name += std::get<0>(*it_varExp);

		if(std::get<1>(*it_varExp) > 1)
		{
			this->name += "^" + std::to_string((long double)(std::get<1>(*it_varExp)));
		}

		this->name += "*";
	}

	// remove last element
	this->name	= this->name.substr(0, this->name.size()-1);
}


//
template<typename SCALAR>
bool Multivariate_Monomial<SCALAR>::operator<(const Multivariate_Monomial& other) const
{
	return this->name < other.name;
}


// 
template<typename SCALAR>
bool Multivariate_Monomial<SCALAR>::operator==(const Multivariate_Monomial& other) const
{
	return this->name == other.name;
}


// prints a monomial, for example x^4yz^3
template<typename SCALAR>
std::ostream& operator<< (std::ostream& argr_o, const Multivariate_Monomial<SCALAR>& argr_mm)
{
	std::set<std::tuple<std::string, int, int>>::const_iterator it_varExp;
	for(it_varExp = argr_mm.factors_list.begin(); it_varExp != argr_mm.factors_list.end(); it_varExp++)
	{
		argr_o << std::get<0>(*it_varExp);
		if(std::get<2>(*it_varExp) > 1)
		{
			argr_o << "^" << std::get<2>(*it_varExp);
		}
	}

	return argr_o;
}


// nothing special to do...
template<typename SCALAR>
std::istream& operator>> (std::istream& argr_i, Multivariate_Monomial<SCALAR>& argr_mm)
{
	return argr_i;
}


} // namespace bbs

#endif