#ifndef _SYSTEM_H_
#define _SYSTEM_H_

/*!
 * \file		System.h
 * \brief		System of constraints
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * File containing the System class, which represents a system of contraints.
 *
 */

#include "Variable.h"
#include "Constraint.h"

/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{

/*! 
 * \class	System
 * \brief	Class representing a system
 *
 *  This class represents internally a system of constraints.
 */
template<typename SCALAR>
class System
{

public:
	/*!
     * \brief Constructeur
	 *
     */
	System();

	/*!
     * \brief Destructor
	 *
     */
	~System();

public:
	std::map<int, Variable<SCALAR>>					v_list;		/*!< List of variables */
	std::map<int, Multivariate_Monomial<SCALAR>>	mm_list;	/*!< List of monomials occuring in the system */
	std::vector<Constraint<SCALAR>>					c_list;		/*!< List of contraints of the system */

	std::map<std::string, int> vToC;	/*!< Mapping between variables and their Ids: monomial -> Id */
	std::map<int, std::string> cToV;	/*!< Mapping between variables and their Ids: Id -> monomial */

	std::map<std::string, int> mmToC;	/*!< Mapping between monomials and their Ids: monomial -> Id */
	std::map<int, std::string> cToMm;	/*!< Mapping between monomials and their Ids: Id -> monomial */

	int nb_var;		/*!< Number of variables of the system */
	int nb_mm;		/*!< Number of monomials in the system. Different from the number of variables. */

public:
	/*!
     * \brief Adding a variable
     *
     * Adds a variable to the system, given its name and bounds.
     *
     * \param argr_varName	: specify a variable name
	 * \param argp_lb		: initial lower bound
	 * \param argp_ub		: initial upper bound
	 *
     */
	void addVariable(std::string arg_varName, SCALAR arg_lb, SCALAR arg_ub);

	/*!
     * \brief Adding a monomial
     *
     * Adds a monomial to the system, if it does not exist yet.
     *
     * \param argr_mm		: monomial to add
	 *
     */
	void addMonomial(Multivariate_Monomial<SCALAR> arg_mm);

	/*!
     * \brief Adding a constraint
     *
     * Adds a constraint to the system, given its name, type, left and right sides.
     *
     * \param arg_name		: specify a name for the constraint
	 * \param arg_type		: type of the constraint
	 * \param argr_leftSide	: New lower bounds
	 * \param argp_newUpper	: New upper bounds
	 *
     */
	void addConstraint(std::string arg_name, int arg_type, Multivariate_Polynomial<SCALAR> arg_leftSide, SCALAR arg_rightSide);

public:
	/*!
     * \brief Get the variable list
     *
     * \return	: Pointer to system variables list
	 *
     */
	inline std::vector<Variable<SCALAR>>* getVarList();

	/*!
     * \brief Get the monomial list
     *
     * \return	: Pointer to system monomials list
	 *
     */
	inline std::vector<Multivariate_Monomial<SCALAR>>* getMonomialList();

	/*!
     * \brief Get the constraint list
     *
     * \return	: Pointer to system constraints list
	 *
     */
	inline std::vector<Constraint<SCALAR>>* getConstraintList();

	/*!
     * \brief Gets variable name by Id
     *
     * Gets variable name by Id
     *
     * \param argr_varId	: Variable Id
	 *
     */
	std::string getVariableName(int arg_varId);

	/*!
     * \brief Gets variable Id by name
     *
     * Gets variable Id by name
     *
     * \param	: Variable name
	 *
     */
	int getVariableId(std::string arg_varName);

	/*!
     * \brief Gets pointer to monomial by Id
     *
     * Get pointer to monomial by id
     *
     * \param	: Monomial id
	 *
     */

	/*!
     * \brief Gets monomial name by Id
     *
     * Gets monomial name by Id
     *
     * \param argr_mmId	: Monomial Id
	 *
     */
	std::string getMonomialName(int arg_mmId);

	/*!
     * \brief Gets monomial Id by name
     *
     * Gets monomial Id by name
     *
     * \param	: Monomial name
	 *
     */
	int getMonomialId(std::string arg_mmName);

	/*!
     * \brief Gets pointer to monomial by Id
     *
     * Get pointer to monomial by id
     *
     * \param	: Monomial id
	 *
     */

	Variable<SCALAR>* getVariableById(std::string arg_varId);

	/*!
     * \brief Gets pointer to monomial by name
     *
     * Gets pointer to monomial by name
     *
     * \param	: Monomial name
	 *
     */
	Variable<SCALAR>* getVariableByName(std::string arg_varName);

	/*!
     * \brief Gets pointer to monomial by Id
     *
     * Get pointer to monomial by id
     *
     * \param	: Monomial id
	 *
     */

	Multivariate_Monomial<SCALAR>* getMonomialById(std::string arg_mmId);

	/*!
     * \brief Gets pointer to monomial by name
     *
     * Gets pointer to monomial by name
     *
     * \param	: Monomial name
	 *
     */
	Multivariate_Monomial<SCALAR>* getMonomialByName(std::string arg_mmName);

public:
	/*!
     * \brief Multivariate_Monomial output operator
     *
     * \param argr_o	: Output stream
	 * \param argr_mm	: Monomial to output
	 *
	 * \return			: Output stream
     */
	template<typename SCALAR>
	friend std::ostream& operator<< (std::ostream& argr_o, const System<SCALAR>& argr_s);

	/*!
     * \brief Multivariate_Monomial input operator
     *
     * \param argr_i	: Input stream
	 * \param argr_mm	: Monomial to fill in from input stream
	 *
	 * \return			: Input stream
     */
	template<typename SCALAR>
	friend std::istream& operator>> (std::istream& argr_i, System<SCALAR>& argr_s);

};


// ################################# //


//
template<typename SCALAR>
System<SCALAR>::System()
{
	this->nb_var	= 0;
	this->nb_mm		= 0;
}


//
template<typename SCALAR>
System<SCALAR>::~System(void)
{
}


// add equation to the problem
// TODO add the corresponding monomial to mm_list
template<typename SCALAR>
void System<SCALAR>::addVariable(std::string arg_varName, SCALAR arg_lb, SCALAR arg_ub)
{
	// add the variable to the variable list
	Variable<SCALAR> var_tmp	= Variable<SCALAR>(arg_varName, arg_lb, arg_ub);
	this->v_list[this->nb_var]	= var_tmp;

	// update the variable mapping
	this->vToC[arg_varName]		= this->nb_var;
	this->cToV[this->nb_var]	= arg_varName;

	// increment nb_var counter
	this->nb_var++;

	// add the corresponding monomial
	Multivariate_Monomial<SCALAR> mm_tmp;
	mm_tmp.addFactor(arg_varName, 1);
	
	// the monomial will be added only if it is not already present
	this->addMonomial(mm_tmp);
}


// add equation to the problem
// TODO add the corresponding monomial to mm_list
template<typename SCALAR>
void System<SCALAR>::addMonomial(Multivariate_Monomial<SCALAR> arg_mm)
{

	// if we do not find the monomial, we add it
	if(this->mmToC.find(arg_mm.name) == this->mmToC.end())
	{
		this->mm_list[this->nb_mm]	= arg_mm;

		this->mmToC[arg_mm.name]	= this->nb_mm;
		this->cToMm[this->nb_mm]	= arg_mm.name;

		// increment here nb_mm, to be sure that simple variables have the first indexes (mandatory for the box for example)
		this->nb_mm++;
	}

}


// add equation to the problem
template<typename SCALAR>
void System<SCALAR>::addConstraint(std::string arg_name, int arg_type, Multivariate_Polynomial<SCALAR> arg_leftSide, SCALAR arg_rightSide)
{
	// tmp constraint
	Constraint<SCALAR> c_tmp = Constraint<SCALAR>(arg_name, arg_type, arg_leftSide, arg_rightSide);

	// add constraint to vector
	this->c_list.push_back(c_tmp);

	// add all the monomials present in the left side
	for(unsigned int i = 0; i < arg_leftSide.mm_list.size(); i++)
	{
		// the monomial will be added only if it is not already present
		this->addMonomial(*arg_leftSide.mm_list[i]);
	}

}


//
template<typename SCALAR>
std::vector<Variable<SCALAR>>* System<SCALAR>::getVarList()
{
	return &(this->v_list);
}


//
template<typename SCALAR>
std::vector<Multivariate_Monomial<SCALAR>>* System<SCALAR>::getMonomialList()
{
	return &(this->mm_list);
}


//
template<typename SCALAR>
std::vector<Constraint<SCALAR>>* System<SCALAR>::getConstraintList()
{
	return &(this->c_list);
}


//
template<typename SCALAR>
std::string System<SCALAR>::getVariableName(int arg_varId)
{
	return this->cToV[arg_varId];
}


//
template<typename SCALAR>
int System<SCALAR>::getVariableId(std::string arg_varName)
{
	return this->vToC[arg_varName];
}

//
template<typename SCALAR>
std::string System<SCALAR>::getMonomialName(int arg_mmId)
{
	return this->mmToV[arg_mmId];
}


//
template<typename SCALAR>
int System<SCALAR>::getMonomialId(std::string arg_mmName)
{
	return this->mmToC[arg_mmName];
}


// 
template<typename SCALAR>
Variable<SCALAR>* System<SCALAR>::getVariableById(std::string arg_varId)
{
	return &(this->v_list[arg_varId]);
}


// 
template<typename SCALAR>
Variable<SCALAR>* System<SCALAR>::getVariableByName(std::string arg_varName)
{
	return &(this->v_list[this->vToC[arg_varName]]);
}


// 
template<typename SCALAR>
Multivariate_Monomial<SCALAR>* System<SCALAR>::getMonomialById(std::string arg_mmId)
{
	return &(this->mm_list[arg_mmId]);
}


// 
template<typename SCALAR>
Multivariate_Monomial<SCALAR>* System<SCALAR>::getMonomialByName(std::string arg_mmName)
{
	return &(this->mm_list[this->mmToC[arg_mmName]]);
}


// print a system
template<typename SCALAR>
std::ostream& operator<<(std::ostream& argr_o, const System<SCALAR>& argr_s)
{
	// print variables
	argr_o << "SYSTEM VARIABLES:         " << endl;
	argr_o << "__________________________" << endl;
	argr_o << endl;

	std::map<int, Variable<SCALAR>>::const_iterator it_v;
	for(it_v = argr_s.v_list.begin(); it_v != argr_s.v_list.end(); it_v++)
	{
		cout << it_v->second << endl;
	}

	argr_o << endl;

	// print constraints
	argr_o << "SYSTEM CONSTRAINTS:       " << endl;
	argr_o << "__________________________" << endl;
	argr_o << endl;

	std::vector<Constraint<SCALAR>>::const_iterator it_c;
	for(it_c = argr_s.c_list.begin(); it_c != argr_s.c_list.end(); it_c++)
	{
		cout << (*it_c) << endl;
	}

	return argr_o;
}


// nothing special to do...
template<typename SCALAR>
std::istream& operator>> (std::istream& argr_i, System<SCALAR>& argr_s)
{
	return argr_i;
}


} // namespace bbs

#endif