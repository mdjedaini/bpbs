#ifndef _AYN_LARGEST_H_
#define _AYN_LARGEST_H_

// inclusion du fichier box.h
#include "Box.h"

// fichier contenu dans la branche "test"
namespace bbs
{

// scalar est pour un ...
template<typename SCALAR>
// je rajoute encore une ligne
class Ayn_Largest
{

public:
	Ayn_Largest(void);
	~Ayn_Largest(void);

public:
	std::vector<std::string> operator()(Box<SCALAR>* argp_b);

};


// ############################ //


//
template<typename SCALAR>
Ayn_Largest<SCALAR>::Ayn_Largest(void)
{
}


//
template<typename SCALAR>
Ayn_Largest<SCALAR>::~Ayn_Largest(void)
{
}


// where to split? give a list of indexes (in most of the case just one index)
template<typename SCALAR>
std::vector<std::string> Ayn_Largest<SCALAR>::operator()(Box<SCALAR>* argp_b)
{
	// prepare the result
	std::vector<std::string> res;

	// indice 
	int res_indice	= 0;

	for(int i = 0; i < argp_b->nb_var; i++)
	{
		// if at least one variable is not stable we do not split
		if(argp_b->width[i] > argp_b->width[res_indice])
		{
			res_indice	= i;
		}
	}

	// TODO change this, it is not complete
	res.push_back(argp_b->s->cToV[res_indice]);

	// return the result
	return res;
}


} // namespace bbs


#endif
