#ifndef _LPADAPTER_LPSOLVE_H_
#define _LPADAPTER_LPSOLVE_H_

#include "Box.h"
#include "System.h"

#include <ClpModel.hpp>
#include <ClpSimplex.hpp>
#include <ClpInterior.hpp>


namespace bbs
{


template<typename SCALAR>
class LPAdapter_LPSOLVE
{

public:
	LPAdapter_LPSOLVE(void);													// default constructor
	LPAdapter_LPSOLVE(LPAdapter_LPSOLVE<SCALAR>* argp_lpa);					// copy constructor
	LPAdapter_LPSOLVE(System<SCALAR>* argp_s, Box<SCALAR>* argp_b);				// create from system
	~LPAdapter_LPSOLVE();

public:
	System<SCALAR>* s;		// pointer to the original system
	Box<SCALAR>* b;			// pointer to the box

	ClpSimplex* model;		// pointer to the lp model

public:
	void setObjective(int arg_minMax, std::vector<std::pair<std::string, SCALAR>> arg_objective);		// 
	void solve();																						// solve the problem

	// internal methods
	void init();																						// called by constructor
	void addVariable(std::string name, int arg_id, SCALAR arg_infBound, SCALAR arg_supBound);			// 
	void addConstraint(Constraint<SCALAR>* argp_c);														// internal call

	// get infos
	int getStatus();						// get status of the solve process
	SCALAR getValue(std::string);			// get value of a given variable
	SCALAR getOptimum();					// return the optimum (effective result)

}; // class LPAdapter_LPSOLVE


// ######################## //


//
template<typename SCALAR>
LPAdapter_LPSOLVE<SCALAR>::LPAdapter_LPSOLVE(void)
{
	this->model	= new ClpSimplex();
	this->model->setLogLevel(0);
}


//
template<typename SCALAR>
LPAdapter_LPSOLVE<SCALAR>::LPAdapter_LPSOLVE(LPAdapter_LPSOLVE* argp_lpa)
{
	// copy the system and the box
	this->s		= argp_lpa->s;
	this->b		= argp_lpa->b;

	this->model	= new ClpSimplex(*(argp_lpa->model));
	//this->model->setLogLevel(0);
}


//
template<typename SCALAR>
LPAdapter_LPSOLVE<SCALAR>::LPAdapter_LPSOLVE(System<SCALAR>* argp_s, Box<SCALAR>* argp_b)
{
	this->s	= argp_s;
	this->b	= argp_b;

	// init the size of the model
	this->init();
}


//
template<typename SCALAR>
LPAdapter_LPSOLVE<SCALAR>::~LPAdapter_LPSOLVE(void)
{

}


// a l'appel de cette fonction, le modele global a deja ete cree par le LP factory
// on doit maintenant completer la construction du modele en creant la fonction objectif
// les arguments sont passes a la fonction init plutot que de les restocker en variable membre
template<typename SCALAR>
void LPAdapter_LPSOLVE<SCALAR>::setObjective(int arg_minMax, std::vector<std::pair<std::string, SCALAR>> arg_objective)
{
	// 1 -> minimize | -1 -> maximize
	if(arg_minMax == -1) this->model->setOptimizationDirection(1);
	if(arg_minMax == 1) this->model->setOptimizationDirection(-1);

	// on cree la fonction objective dans le format desire par CLP
	std::vector<std::pair<std::string, SCALAR>>::iterator it_ao;
	for(it_ao = arg_objective.begin(); it_ao != arg_objective.end(); it_ao++)
	{
		// get the id of the current monomial
		int varId	= this->s->vToC[it_ao->first];

		// add coeff in objective (id of variable + coefficient)
		this->model->setObjectiveCoefficient(varId, it_ao->second);
	}

}


// ici je cree le modele pour COIN_OR_CLP je ne suis plus dans la genericite...
template<typename SCALAR>
void LPAdapter_LPSOLVE<SCALAR>::init()
{
	// number of columns of the model
	int nbc	= this->s->mm_list.size();

	//
	this->model	= new ClpSimplex();
	
	// set log level of the system
	this->model->setLogLevel(0);

	// number of columns = total number of variables
	// bien mettre 0 pour le nombre de lignes! celui ci sera incremente au fur et a mesure de l'ajout des contraintes
	this->model->resize(0, nbc);
}


//
//
template<typename SCALAR>
void LPAdapter_LPSOLVE<SCALAR>::addVariable(std::string name, int arg_id, SCALAR arg_infBound, SCALAR arg_supBound)
{
	this->model->setColumnLower(arg_id, arg_infBound.lowerBound());
	this->model->setColumnUpper(arg_id, arg_supBound.upperBound());
}

//
template<typename SCALAR>
void LPAdapter_LPSOLVE<SCALAR>::addConstraint(Constraint<SCALAR>* argp_c)
{

	// number of elements, i.e. # of monomials in the left side of the constraint
	int nb_elem			= argp_c->leftSide.mm_list.size();

	// prepare the data to be given to COIN OR
	int* rowIndex		= new int[nb_elem];

	// here there is no templatization, CLP only accept double
	double* rowValue	= new double[nb_elem];

	int cursor	= 0;

	std::vector<std::pair<SCALAR, Multivariate_Monomial<SCALAR>>>::iterator it_mm;
	for(it_mm = argp_c->leftSide.mm_list.begin(); it_mm != argp_c->leftSide.mm_list.end(); it_mm++)
	{
		// get the id for the current monomial
		rowIndex[cursor]	= this->s->vToC[(it_mm->second).name];

		// coefficient is first in the pair
		rowValue[cursor]	= it_mm->first;

		// increment cursor
		cursor++;
	}

	if(argp_c->type == CONSTRAINT_LESS_THAN)
	{
		this->model->addRow(nb_elem, rowIndex, rowValue, -COIN_DBL_MAX, argp_c->rightSide);
	}
	else if(argp_c->type == CONSTRAINT_MORE_THAN)
	{
		this->model->addRow(nb_elem, rowIndex, rowValue, argp_c->rightSide, COIN_DBL_MAX);
	}
	else if(argp_c->type == CONSTRAINT_EQUAL_TO)
	{
		this->model->addRow(nb_elem, rowIndex, rowValue, argp_c->rightSide, argp_c->rightSide);
	}

}


// solve with dual simplex
template<typename SCALAR>
void LPAdapter_LPSOLVE<SCALAR>::solve()
{
	//this->model->writeMps("D:\\Users\\mdjedaini\\Documents\\MD_Projects\\PROJET - Contraintes Geometriques\\test\\lp.mps");

	// solving time, we specialize...
	this->model->primal();
}


// return the status (processing, end of process...)
template<typename SCALAR>
int LPAdapter_LPSOLVE<SCALAR>::getStatus()
{
	return this->model->status();
}


// return the status (processing, end of process...)
template<typename SCALAR>
SCALAR LPAdapter_LPSOLVE<SCALAR>::getValue(std::string arg_varName)
{
	//return this->model->getObjValue();
	return *(this->model->getColSolution());
}


// return the optimum (effective result)
template<typename SCALAR>
SCALAR LPAdapter_LPSOLVE<SCALAR>::getOptimum()
{
	return (SCALAR)this->model->getObjValue();
}


} // namespace bbs

#endif

