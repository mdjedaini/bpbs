#ifndef _MYSCALAR_H_
#define _MYSCALAR_H_

/*!
 * \file		LPFactory.h
 * \brief		Programme de tests.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * Programme de test pour l'objet de gestion des cha�nes de caract�res Str_t.
 *
 */

// include boost interval arithmetic header
#include <boost/numeric/interval.hpp>

namespace bbs
{

/*! 
 * \class	BBS_Solver
 * \brief	BBS_Solver class is responsible for solving the system it contains as a member variable.
 *
 * BBS_Solver is responsible for solving multivariate polynomials systems.
 * The system is given as a member variable of this class.
 * The Solution member is filled throughout the solving process.
 */
template<typename SCALAR>
class MyScalar
{

public:
	MyScalar(void);												// 
	MyScalar(SCALAR arg_double);								// 
	~MyScalar(void);											// 

public:
	SCALAR internal_value;										// 
	boost::numeric::interval<SCALAR> interval;					// 

public:
	SCALAR lowerBound();
	SCALAR upperBound();

public:
	MyScalar<SCALAR>& operator=	(MyScalar<SCALAR> const &argr_rightSide);
	bool operator<		(MyScalar<SCALAR> const &argr_rightSide);
	bool operator<=		(MyScalar<SCALAR> const &argr_rightSide);
	bool operator>		(MyScalar<SCALAR> const &argr_rightSide);
	bool operator>=		(MyScalar<SCALAR> const &argr_rightSide);

	MyScalar<SCALAR> operator+	(MyScalar<SCALAR> const &argr_rightSide);
	MyScalar<SCALAR> operator-	(MyScalar<SCALAR> const &argr_rightSide);
	MyScalar<SCALAR> operator*	(MyScalar<SCALAR> const &argr_rightSide);
	MyScalar<SCALAR> operator/	(MyScalar<SCALAR> const &argr_rightSide);

	MyScalar<SCALAR>& operator+= (MyScalar<SCALAR> const &argr_rightSide);
	MyScalar<SCALAR>& operator-= (MyScalar<SCALAR> const &argr_rightSide);
	MyScalar<SCALAR>& operator*= (MyScalar<SCALAR> const &argr_rightSide);
	MyScalar<SCALAR>& operator/= (MyScalar<SCALAR> const &argr_rightSide);

// conversion operator
public:
	operator SCALAR() const;

public:
	/*!
     * \brief Multivariate_Monomial output operator
     *
     * \param argr_o	: Output stream
	 * \param argr_mm	: Monomial to output
	 *
	 * \return			: Output stream
     */
	template<typename SCALAR>
	friend std::ostream& operator<< (std::ostream& argr_o, const MyScalar& argr_d);		// 

	/*!
     * \brief Multivariate_Monomial input operator
     *
     * \param argr_i	: Input stream
	 * \param argr_mm	: Monomial to fill in from input stream
	 *
	 * \return			: Input stream
     */
	template<typename SCALAR>
	friend std::istream& operator>> (std::istream& argr_i, MyScalar& argr_d);			// 

};


// ############################ //


// 
template<typename SCALAR>
MyScalar<SCALAR>::MyScalar(void)
{
}


// 
template<typename SCALAR>
MyScalar<SCALAR>::MyScalar(SCALAR arg_d)
{
	this->internal_value	= arg_d;
	this->interval			= boost::numeric::interval<SCALAR>(this->internal_value);
}


// 
template<typename SCALAR>
MyScalar<SCALAR>::~MyScalar(void)
{
}


// 
template<typename SCALAR>
SCALAR MyScalar<SCALAR>::lowerBound()
{
	return this->interval.lower();
}


// 
template<typename SCALAR>
SCALAR MyScalar<SCALAR>::upperBound()
{
	return this->interval.upper();
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
MyScalar<SCALAR>& MyScalar<SCALAR>::operator=(MyScalar<SCALAR> const &argr_rightSide)
{

	// add internal value together
	this->internal_value	= argr_rightSide.internal_value;

	// add intervals together
	this->interval			= argr_rightSide.interval;

	return *this;
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
bool MyScalar<SCALAR>::operator< (MyScalar<SCALAR> const &argr_rightSide)
{
	return (this->internal_value < argr_rightSide.internal_value);
	//return (this->interval < argr_rightSide.interval);
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
bool MyScalar<SCALAR>::operator<= (MyScalar<SCALAR> const &argr_rightSide)
{
	return (this->internal_value <= argr_rightSide.internal_value);
	//return (this->interval <= argr_rightSide.interval);
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
bool MyScalar<SCALAR>::operator> (MyScalar<SCALAR> const &argr_rightSide)
{
	return (this->internal_value > argr_rightSide.internal_value);
	//return (this->interval > argr_rightSide.interval);
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
bool MyScalar<SCALAR>::operator>= (MyScalar<SCALAR> const &argr_rightSide)
{
	return (this->internal_value >= argr_rightSide.internal_value);
	//return (this->interval >= argr_rightSide.interval);
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
MyScalar<SCALAR> MyScalar<SCALAR>::operator+ (MyScalar<SCALAR> const &argr_rightSide)
{
	// result to be returned
	MyScalar<SCALAR> res;

	// add internal value together
	res.internal_value	= this->internal_value + argr_rightSide.internal_value;

	// add intervals together
	res.interval		= this->interval + argr_rightSide.interval;

    return res;
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
MyScalar<SCALAR> MyScalar<SCALAR>::operator- (MyScalar<SCALAR> const &argr_rightSide)
{
	// result to be returned
	MyScalar<SCALAR> res;

	// add internal value together
	res.internal_value	= this->internal_value - argr_rightSide.internal_value;

	// add intervals together
	res.interval		= this->interval - argr_rightSide.interval;
    
	return res;
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
MyScalar<SCALAR> MyScalar<SCALAR>::operator* (MyScalar<SCALAR> const &argr_rightSide)
{
	// result to be returned
	MyScalar<SCALAR> res;

	// add internal value together
	res.internal_value	= this->internal_value * argr_rightSide.internal_value;

	// add intervals together
	res.interval		= this->interval * argr_rightSide.interval;
    
	return res;
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
MyScalar<SCALAR> MyScalar<SCALAR>::operator/ (MyScalar<SCALAR> const &argr_rightSide)
{
	// result to be returned
	MyScalar<SCALAR> res;

	// add internal value together
	res.internal_value	= this->internal_value / argr_rightSide.internal_value;

	// add intervals together
	res.interval		= this->interval / argr_rightSide.interval;
    
	return res;
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
MyScalar<SCALAR>& MyScalar<SCALAR>::operator+= (MyScalar<SCALAR> const &argr_rightSide)
{
	// add internal value together
	this->internal_value	+= argr_rightSide.internal_value;

	// add intervals together
	this->interval			+= argr_rightSide.interval;

    return *this;
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
MyScalar<SCALAR>& MyScalar<SCALAR>::operator-= (MyScalar<SCALAR> const &argr_rightSide)
{
	// add internal value together
	this->internal_value	-= argr_rightSide.internal_value;

	// add intervals together
	this->interval			-= argr_rightSide.interval;
    
	return *this;
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
MyScalar<SCALAR>& MyScalar<SCALAR>::operator*= (MyScalar<SCALAR> const &argr_rightSide)
{
	// add internal value together
	this->internal_value	*= argr_rightSide.internal_value;

	// add intervals together
	this->interval			*= argr_rightSide.interval;
    
	return *this;
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
MyScalar<SCALAR>& MyScalar<SCALAR>::operator/= (MyScalar<SCALAR> const &argr_rightSide)
{
	// add internal value together
	this->internal_value	/= argr_rightSide.internal_value;

	// add intervals together
	this->interval			/= argr_rightSide.interval;
    
	return *this;
}


//
template<typename SCALAR>
MyScalar<SCALAR>::operator SCALAR() const
{
	return this->internal_value;
}


// print a monomial, for example x^4yz^3
template<typename SCALAR>
std::ostream& operator<< (std::ostream& argr_o, const MyScalar<SCALAR>& argr_d)
{
	//
	//argr_o << "Internal value: " << argr_d.internal_value << std::endl;
	argr_o << argr_d.internal_value;

	//
	//argr_o << "[" << argr_d.interval.lower() << ", " << argr_d.interval.upper() << std::endl;

	return argr_o;
}


// nothing special to do...
template<typename SCALAR>
std::istream& operator>> (std::istream& argr_i, MyScalar<SCALAR>& argr_d)
{
	// put the double that we read in the internal value
	argr_i >> argr_d.internal_value;

	// create the interval for this internal value
	argr_d.interval	= boost::numeric::interval<SCALAR>(argr_d.internal_value);

	return argr_i;
}


} // namespace bbs


#endif
