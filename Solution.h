#ifndef _SOLUTION_H_
#define _SOLUTION_H_

/*!
 * \file		LPFactory.h
 * \brief		Programme de tests.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * Programme de test pour l'objet de gestion des cha�nes de caract�res Str_t.
 *
 */

#include "Box.h"

/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{

/*! 
 * \class	Solution
 * \brief	Class for embedding solutions of a system
 *
 * 
 */
template<typename SCALAR>
class Solution
{

public:
	
	/*!
     * \brief Default constructor
	 *
     */
	Solution();

	/*!
     * \brief Constructor with solution precision
	 *
     */
	Solution(SCALAR* argp_solution_precision);

	/*!
     * \brief Destructor
	 *
     */
	~Solution();

public:
	/*!
     * \brief Solution precision
	 * 
	 * The solution precision defines the minimum distance between boxes.
     */	
	SCALAR* solution_precision;

	/*!
     * \brief Solution precision
	 * 
	 * The solution precision defines the 
     */
	std::vector<Box<SCALAR>> sol;

	/*!
     * \brief Garbage solutions
	 * 
	 * The solution precision defines the 
     */
	std::vector<Box<SCALAR>> garbage;

	/*!
     * \brief Garbage solutions
	 * 
	 * The solution precision defines the 
     */
	int nb_TreatedBoxes;

	/*!
     * \brief Logs a message
	 *
	 * 
	 * 
     */
	int nb_EmptyBoxes;

	/*!
     * \brief Logs a message
	 *
	 * 
	 * 
     */
	int nb_Splits;

	/*!
     * \brief Logs a message
	 *
	 * 
	 * 
     */
	double elapsed_time;

public:

	/*!
     * \brief Adds a box to the solution
     *
     * Responsible for adding a box to the list of solutions. Prevents from adding twice the same box.
     *
	 * \param arg_b			: Box to add to the solution
	 *
	 * \return				: 0 if the box goes to garbage, 1 if box is added to the solution set
	 *
     */
	int addBox(Box<SCALAR> arg_b);

//
public:
	template<typename SCALAR>
	friend std::ostream& operator<< (std::ostream& argr_o, const Solution<SCALAR>& argr_s);  // output operator

	template<typename SCALAR>
	friend std::istream& operator>> (std::istream& argr_i, Solution<SCALAR>& argr_s);        // input operator

}; // class Solution


// ###################### //


//
template<typename SCALAR>
Solution<SCALAR>::Solution()
{
}


//
template<typename SCALAR>
Solution<SCALAR>::Solution(SCALAR* argp_solution_precision)
{
	this->solution_precision;
}


//
template<typename SCALAR>
Solution<SCALAR>::~Solution()
{
	//std::to_string(this->sol->size());
}


//
template<typename SCALAR>
int Solution<SCALAR>::addBox(Box<SCALAR> arg_b)
{

	// if box is not found in this->sol we add it (box overrides == operator)
	if(std::find(this->sol.begin(), this->sol.end(), arg_b) == this->sol.end())
	{
		this->sol.push_back(arg_b);
		return 1;
	}

	// if we are not interested by the solution, we add it to the garbage
	else
	{
		this->garbage.push_back(arg_b);
		return 0;
	}
}


// print a system
template<typename SCALAR>
std::ostream& operator<< (std::ostream& argr_o, const Solution<SCALAR>& argr_s)
{
	argr_o << endl;

	// print variables
	argr_o << "There is " << argr_s.sol.size() << " final solution(s)..." << endl;
	argr_o << "__________________________" << endl;

	argr_o << endl;

	// print the boxes independently
	std::vector<Box<SCALAR>>::const_iterator it_b;
	for(it_b = argr_s.sol.begin(); it_b != argr_s.sol.end(); it_b++)
	{
		argr_o << (*it_b) << endl;
	}

	//
	argr_o << endl;

	// print variables
	argr_o << argr_s.garbage.size() << " garbage solution(s):" << endl;
	argr_o << "__________________________" << endl;

	argr_o << endl;

	// print the boxes independently
	//std::vector<Box<SCALAR>>::const_iterator it_b;
	for(it_b = argr_s.garbage.begin(); it_b != argr_s.garbage.end(); it_b++)
	{
		argr_o << (*it_b) << endl;
	}

	return argr_o;
}


// nothing special to do...
template<typename SCALAR>
std::istream& operator>> (std::istream& argr_i, Solution<SCALAR>& argr_s)
{
	return argr_i;
}


}


#endif