#ifndef _CHRONO_H_
#define _CHRONO_H_

/*!
 * \file		Bernstein.h
 * \brief		Bernstein class
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * File containing the Bernstein class, responsible for generating Bernstein constraints.
 *
 */

#include <time.h>

/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{


/*! 
 * \class	Chrono
 * \brief	Chrono class is responsible for picking-up the time.
 *
 * This class is responsible for solving multivariate polynomials systems. The solution is contained as a member variable.
 */
class Chrono
{

//
public:
	/*!
     * \brief Default constructor
     *
     */
	Chrono(void);

	/*!
     * \brief Destructor
     *
     */
	~Chrono(void);

public:
	/*!
     * \brief Reference time
     *
     */
	static clock_t start;

	/*!
     * \brief Time elapsed between
     *
     */
	double elapsed_time;
	
//
public:
	/*!
     * \brief Initialize the chronometer by setting start to the current time.
     *
     */
	void init();
	
	/*!
     * \brief Get the time elapsed between the function call and the chronometer initialization.
     *
     */
	double getElapsedTime();

};


} // namespace bbs

#endif
