////////////////////////////////////////////////////////////////////////////////
//
// File:            spirit_evaluator.c++
// Purpose:         Arithmetic Expression Parsing and Evaluation
// 
// Original Author: David Bergman http://blog.davber.com/about
// Second Author:   Joel Young <jdy@cs.brown.edu>
//
// License:         MIT
// Copyright:       See Below
//
////////////////////////////////////////////////////////////////////////////////
//
// History:
//   2008.02.27:
//     -- Added support for right-to-left operators for exponentiation
//        and negation
//     -- Added expect clauses to the grammar to throw errors for typos
//     -- Added outputs for postfix, prefix, and tree, and XML notation
//     -- Added detection for divide by zero and fractional roots of
//        negatives
//     -- Added compilation mode for command line argument version
//     -- Changed program name from parse_arith.cpp to spirit_evaluator.c++
//   2008.02.27:
//     -- David Bergman releases under MIT license
//     -- see comment at URL below
//   2006.07.06:
//     -- David Bergman posted original version on his blog
//     -- URL: http://blog.davber.com/2006/07/06/the-spirit-of-parsing/
//
////////////////////////////////////////////////////////////////////////////////
// 
// This program implements a simple arithmetic language using Boost.Spirit.
// 
// It does so by creating an AST and then implementing an evaluating visitor 
// for such nodes.  It also outputs the expression in infix, postfix,
// and prefix notation.  Furthermore it prints an xml coded version of
// the AST as well as a textual representation of the tree. 
//
// (It's not really a visitor, since it handles the traversal itself) 
//
// Compile with:
//
// g++ -o spirit_evaluator spirit_evaluator.c++ -O2
////////////////////////////////////////////////////////////////////////////////
// 
// Copyright (c) 2006-2008 David Bergman
// Copyright (c)      2008 Joel Young
// 
// License Information (MIT License):
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//
// Compile flags:
//
// Uncomment to enable parse time exceptions for consecutive 
// operands such as (6 6) or ((3-5)(5x6)).  Uncommenting will
// result in a 50x slowdown.  Also, note that this expression
// error will result in an incomplete parse and is checked
// for post-parse.
//#define CALC_DETECT_CONSECUTIVE_OPERANDS
//
////////
//
// Uncomment to enable commandline mode for use in scripts:
//#define CALC_SHELL_MODE
// You can then put lines like:
// c () { ~/bin/spirit_evaluator "$*" 1000; }
// t () { ~/bin/spirit_evaluator "$*" 10 1; }
// in your .bashrc to have a handy command line calculator after you
// copy your executable into your bin directory. 
// c 6x6^5 
// outputs
// 46656
// and 
// t 6x6^5
// outputs
// 46656
//   x      
//  / \__   
// 6     ^  
//      / \ 
//     6   5
// 
////////
//
// Uncomment to enable debugging output from spirit's parse system:
//#define BOOST_SPIRIT_DEBUG
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

// Standard Includes:
#include "spirit_evaluator.h"

//using namespace std;
//using namespace boost::spirit;
//using namespace boost;

// Assertions to use during the parse:
boost::spirit::classic::assertion<Errors> expect_close(close_expected);
boost::spirit::classic::assertion<Errors> expect_expression(expression_expected);

