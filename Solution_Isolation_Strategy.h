#ifndef _SOLUTION_ISOLATION_STRATEGY_H_
#define _SOLUTION_ISOLATION_STRATEGY_H_

/*!
 * \file		Solution.h
 * \brief		Programme de tests.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * Programme de test pour l'objet de gestion des cha�nes de caract�res Str_t.
 *
 */

/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{

/*! 
 * \class	Solution_Isolation_Strategy
 * \brief	Box class contains the bounds of system variables, computed at each iteration.
 *
 * 
 */
template<typename SHRINKER, typename HAL, typename AYN, typename KAYF>
class Solution_Isolation_Strategy
{

public:
	/*!
     * \brief Default constructor
     *
     * Default constructor creates an empty box.
     */
	Solution_Isolation_Strategy();

	/*!
     * \brief Default constructor
     *
     * Default constructor creates an empty box.
     */
	~Solution_Isolation_Strategy();

public:
	/*!
     * \brief Default constructor
     *
     * Default constructor creates an empty box.
     */
	SHRINKER*	shrinker;

	/*!
     * \brief Default constructor
     *
     * Default constructor creates an empty box.
     */
	HAL*		hal;

	/*!
     * \brief Default constructor
     *
     * Default constructor creates an empty box.
     */
	AYN*		ayn;

	/*!
     * \brief Default constructor
     *
     * Default constructor creates an empty box.
     */
	KAYF*		kayf;

};


// ######################## //


//
template<typename SHRINKER, typename HAL, typename AYN, typename KAYF>
Solution_Isolation_Strategy<SHRINKER, HAL, AYN, KAYF>::Solution_Isolation_Strategy(void)
{
	//
	this->shrinker	= new SHRINKER();
	
	//
	this->hal		= new HAL();

	//
	this->ayn		= new AYN();

	//
	this->kayf		= new KAYF();
}


//
template<typename SHRINKER, typename HAL, typename AYN, typename KAYF>
Solution_Isolation_Strategy<SHRINKER, HAL, AYN, KAYF>::~Solution_Isolation_Strategy(void)
{
	delete this->shrinker;
	delete this->hal;
	delete this->ayn;
	delete this->kayf;
}


}


#endif