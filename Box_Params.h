#ifndef _BOX_PARAMS_H_
#define _BOX_PARAMS_H_

/*!
 * \file		Box_Params.h
 * \brief		File containing the Box class. which contains the variable bounds computed at each iteration.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * File containing the Box class, which contains the variable bounds computed at each iteration.
 *
 */

/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{

/*! 
 * \class	Box_Params
 * \brief	Box_Params is used to easily transfer parameters from solver to boxes.
 *
 * 
 */
template<typename SCALAR>
class Box_Params
{

public:
	/*!
     * \brief Default Box_Params constructor
     *
     * Creates a copy of the provided box.
	 *
     */
	Box_Params();

	/*!
     * \brief Copy constructor
     *
     * Creates a copy of the provided box.
	 *
     */
	~Box_Params();

public:
	
	/*!
     * \brief Wanted accuracy, used for solution test
	 *
     * The wanted accuracy epsilon is used when checking whether a box is a solution or not.
	 * If all interval widths within a box are smaller than epsilon, them we conclude that they are accurate enough.
	 * The box is henceforth considered as a solution.
	 *
     */
	SCALAR* epsilon;
	
	/*!
     * \brief Precision of a box, used for equality test
     *
     */
	SCALAR* precision;

	/*!
     * \brief Stability criteria, used to consider box as stable or not
     *
     */
	SCALAR* stability_criteria;

	/*!
     * \brief Pointer to the system mapping
     *
     */
	std::map<std::string, int>* vToC;
	
	/*!
     * \brief Pointer to the system mapping
     *
     */
	std::map<int, std::string>* cToV;

};


//


//
template<typename SCALAR>
Box_Params<SCALAR>::Box_Params()
{

}


//
template<typename SCALAR>
Box_Params<SCALAR>::~Box_Params()
{

}


} // namespace bbs

#endif
