#ifndef _PARAMS_H_
#define _PARAMS_H_

/*!
 * \file		LPFactory.h
 * \brief		Programme de tests.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * Programme de test pour l'objet de gestion des cha�nes de caract�res Str_t.
 *
 */

#include "Solution_Isolation_Strategy.h"
#include "LPFactory.h"
#include "Logger.h"
#include "Chrono.h"

namespace bbs
{

/*! 
 * \class	BBS_Solver
 * \brief	BBS_Solver class is responsible for solving the system it contains as a member variable.
 *
 * BBS_Solver is responsible for solving multivariate polynomials systems.
 * The system is given as a member variable of this class.
 * The Solution member is filled throughout the solving process.
 */
template<typename SCALAR, typename SOLUTION_ISOLATION_STRATEGY, typename LP_FACTORY>
class Params
{

public:
	/*!
     * \brief Wanted accuracy
	 *
     */
	Params();

	/*!
     * \brief Wanted accuracy
	 *
     */
	~Params();

public:
	/*!
     * \brief Wanted accuracy
	 *
     */
	SCALAR wanted_accuracy;

	/*!
     * \brief Solution precision
	 *
     */	
	SCALAR precision;

	/*!
     * \brief Stability criteria, used to consider box as stable or not
     *
     */
	SCALAR stability_criteria;

	/*!
     * \brief Box gap
	 *
     */
	SCALAR box_gap;

	/*!
     * \brief Pointer to the LPFactory which is used to solve
	 *
     */
	SOLUTION_ISOLATION_STRATEGY* sis;

	/*!
     * \brief Pointer to the LPFactory which is used to solve
	 *
     */
	LP_FACTORY* lpf;

	/*!
     * \brief Pointer to the LPFactory which is used to solve
	 *
     */
	Logger<SCALAR>* logger;

	/*!
     * \brief Chronometer, to get the computation time whenever we want
	 *
     */
	Chrono* chrono;

};


// ####################### //


//
template<typename SCALAR, typename SOLUTION_ISOLATION_STRATEGY, typename LP_FACTORY>
Params<SCALAR, SOLUTION_ISOLATION_STRATEGY, LP_FACTORY>::Params()
{
	this->sis		= new SOLUTION_ISOLATION_STRATEGY();
	this->lpf		= new LP_FACTORY();
}


//
template<typename SCALAR, typename SOLUTION_ISOLATION_STRATEGY, typename LP_FACTORY>
Params<SCALAR, SOLUTION_ISOLATION_STRATEGY, LP_FACTORY>::~Params()
{
}


} // namespace bbs

#endif