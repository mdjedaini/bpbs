The Bernstein polytopes based solver (called BBS below) objective is to solve multivariate
polynomial systems of equations.

This file contains informations on how to compile and use the current version of the bernstein 
polytopes based solver (bbs).

#################################################
-------------------------------------------------
------------------- LIBRARIES -------------------
-------------------------------------------------
#################################################

BBS is dependent on the following libraries:

1/ Boost spirit parser, part of the boost library

The boost spirit parser is used in our program to parse system files, from which we build the 
internal representation of the system.

The version of boost used for the initial implementation is boost 1.46.0.
	
For adding boost spirit to the project, follow these steps:

	- download sources from ...
	- define an environment variable called BOOST_ROOT containing the path of the boost main 
	directory

2/ Symbolic C++

Symbolic C++ is a library for symbolic (for formal) computation. It is used in our solver to develop
and reduce the constraints of the system, after the system has been loaded, and before we build the
internal representation of the system.

The version of symbolic C++ used for the initial implementation is SymbolicC++ 3-3.35
	
For adding Symbolic C++ to the project, follow these steps:

	- download Symbolic C++ sources from this page http://issc.uj.ac.za/symbolic/symbolic.html
	- unzip the project directory and place it wherever you want on your computer
	- define an environment variable called SYMBOLICCPP_ROOT containing the location chosen above

2/ 	Coin-OR CLP

Coin-OR CLP is a library for solving linear problems. In our solver, it is used to solve linear 
problems resulting from the linearisation process.

The version of Coin-OR CLP used is Coin-OR Clp-1.15.2
	
For adding Coin-OR CLP to the project, follow these steps:

	- download sources from this location http://www.coin-or.org/download/source/Clp/
	- unzip the project directory and place it wherever you want on your computer
	- define an environment variable called COINOR_ROOT containing the location chosen above
	
	
	
	