#ifndef _MULTIVARIATE_POLYNOMIAL_H_
#define _MULTIVARIATE_POLYNOMIAL_H_

/*!
 * \file		Multivariate_Polynomial.h
 * \brief		File containing the Multivariate_Polynomial class, responsible for representing internally multivariate polynomials.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * File containing the Multivariate_Polynomial class, responsible for representing internally multivariate polynomials.
 *
 */

#include "Multivariate_Monomial.h"


/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{

/*! 
 * \class	Multivariate_Polynomial
 * \brief	Multivariate_Polynomial class internally figures out multivariate polynomials.
 *
 * This class is responsible for internally figuring out multivariate polynomials.
 * It contains a list of coefficients and a list of pointer to monomials, indexed similarly.
 * It also contains the degree of the polynomial, which is computed internally.
 */
template<typename SCALAR>
class Multivariate_Polynomial
{

public:
	/*!
     * \brief Default constructor
	 *
     * Creates an empty Multivariate_Polynomial object, and initialize the degree to 0.
     */
	Multivariate_Polynomial();

	/*!
     * \brief Destructor
     *
	 * Destructor does not need to delete anything.
     */
	~Multivariate_Polynomial();

public:
	/*!
     * \brief List of coefficients
     *
     */
	std::vector<SCALAR> mm_coeff_list;

	/*!
     * \brief List of pointers to monomials
     *
     */
	std::vector<Multivariate_Monomial<SCALAR>*> mm_list;

	/*!
     * \brief Degree of the polynomial, computed internally
     *
     */
	int degree;

public:
	/*!
     * \brief Adds a monomial to the current polynomial
     *
     * Use this function to set the objective function of the current LP model.
     *
     * \param arg_coeff		: Monomial coefficient
	 * \param argp_mm		: Pointer to monomial object.
	 *
     */
	void addMonomial(SCALAR arg_coeff, Multivariate_Monomial<SCALAR>* argp_mm);

public:
	/*!
     * \brief Multivariate_Polynomial output operator
     *
     * \param argr_i	: Output stream
	 * \param argr_mm	: Multivariate_Polynomial to output
	 *
	 * \return			: Output stream
     */
	template<typename SCALAR>
	friend std::ostream& operator<< (std::ostream& argr_o, const Multivariate_Polynomial<SCALAR>& argr_mp);  // output operator

	/*!
     * \brief Multivariate_Monomial input operator
     *
     * \param argr_i	: Input stream
	 * \param argr_mm	: Monomial to fill in from input stream
	 *
	 * \return			: Input stream
     */
	template<typename SCALAR>
	friend std::istream& operator>> (std::istream& argr_i, Multivariate_Polynomial<SCALAR>& argr_mp);        // input operator

};


// ############# //


//
template<typename SCALAR>
Multivariate_Polynomial<SCALAR>::Multivariate_Polynomial()
{
	this->degree	= 0;
}


//
template<typename SCALAR>
Multivariate_Polynomial<SCALAR>::~Multivariate_Polynomial()
{
}


//
template<typename SCALAR>
void Multivariate_Polynomial<SCALAR>::addMonomial(SCALAR arg_coeff, Multivariate_Monomial<SCALAR>* argp_mm)
{
	//
	this->mm_coeff_list.push_back(arg_coeff);

	//
	this->mm_list.push_back(argp_mm);

	// update the degree of the mp if necessary
	if(argp_mm->degree > this->degree)
	{
		this->degree	= argp_mm->degree;
	}
}


// print a multivariate polynomial
template<typename SCALAR>
std::ostream& operator<< (std::ostream& argr_o, const Multivariate_Polynomial<SCALAR>& argr_mp)
{
	int size	= argr_mp.mm_list.size();

	for(int i = 0; i < size; i++) {
		argr_o << argr_mp.mm_coeff_list[i] << (argr_mp.mm_list[i])->name << " ";
	}

	return argr_o;
}


// nothing special to do...
template<typename SCALAR>
std::istream& operator>> (std::istream& argr_i, Multivariate_Polynomial<SCALAR>& argr_mp)
{
	return argr_i;
}


} // namespace bbs

#endif