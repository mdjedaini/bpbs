// ce fichier contient les typedefs
#include "stdafx.h"

// include boost interval arithmetic header
//#include <boost/numeric/interval.hpp>

#include "MyScalar.h"

#include "LPAdapter_COIN_OR_CLP.h"
#include "LPAdapter_LPSOLVE.h"

#include "Shrinker_Default.h"

#include "Hal_Box_Stability.h"
#include "Hal_Variable_Stability.h"
#include "Hal_Always.h"

#include "Ayn_Largest.h"
#include "Ayn_Single_Blocking_Variable.h"

#include "Kayf_Default.h"

#include "Solution_Isolation_Strategy.h"

#include "BBS_Solver.h"

using namespace bbs;


//typedef MyScalar<double>													scalar_type;
typedef double																scalar_type;

typedef LPAdapter_COIN_OR_CLP<scalar_type>									lp_type;

typedef Bernstein<lp_type, scalar_type>										bernstein_type;
typedef LPFactory<lp_type, scalar_type>										lpf_type;

typedef Shrinker_Default<scalar_type, lpf_type>								shrinker_type;
typedef Hal_Box_Stability<scalar_type>										hal_type;
typedef Ayn_Largest<scalar_type>											ayn_type;
typedef Kayf_Default<scalar_type>											kayf_type;

typedef Solution_Isolation_Strategy<shrinker_type, hal_type, ayn_type, kayf_type> sis_type;

typedef BBS_Solver<scalar_type, sis_type, lpf_type>							bbs_type;
typedef Params<scalar_type, sis_type, lpf_type>								params_type;
