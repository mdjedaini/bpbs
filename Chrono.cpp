#include "stdafx.h"
#include "Chrono.h"

using namespace bbs;


//
Chrono::Chrono(void)
{
	this->start = clock();
}


//
Chrono::~Chrono(void)
{
}


//
void Chrono::init()
{
	this->elapsed_time	= 0;
	this->start			= clock();
}


//
double Chrono::getElapsedTime()
{
	return (double)(clock() - this->start) / (double)CLOCKS_PER_SEC;
}