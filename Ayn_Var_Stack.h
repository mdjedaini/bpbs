#ifndef _AYN_VAR_STACK_H_
#define _AYN_VAR_STACK_H_

#include "Box.h"

namespace bbs
{


template<typename SCALAR>
class Ayn_Var_Stack
{

public:
	Ayn_Var_Stack(void);
	~Ayn_Var_Stack(void);

public:
	std::vector<int> operator()(Box<SCALAR>* argp_b);

};


// ###################### //


//
template<typename SCALAR>
Ayn_Var_Stack<SCALAR>::Ayn_Var_Stack(void)
{
}


//
template<typename SCALAR>
Ayn_Var_Stack<SCALAR>::~Ayn_Var_Stack(void)
{
}


// where to split? give a list of indexes (in most of the case just one index)
template<typename SCALAR>
std::vector<int> Ayn_Var_Stack<SCALAR>::operator()(Box<SCALAR>* argp_b)
{
	std::vector<int> res;

	std::vector<int> sv	= argp_b->getStableVariables();

	// sort the variables list
	std::random_shuffle(sv.begin(), sv.end());

	// must exist
	res.push_back(sv[0]);

	/*
	for(unsigned int i = 0; i < argp_b->infos.var_stack.size(); i++)
	{
		if(std::find(sv.begin(), sv.end(), argp_b->infos.var_stack[i]) != sv.end())
		{
			res.push_back(argp_b->infos.var_stack[i]);
			break;
		}
	}
	*/

	return res;
}


} // namespace bbs


#endif
