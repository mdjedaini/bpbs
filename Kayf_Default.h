#ifndef _KAYF_DEFAULT_H_
#define _KAYF_DEFAULT_H_

#include "Box.h"

namespace bbs
{


template<typename SCALAR>
class Kayf_Default
{

public:
	Kayf_Default(void);
	~Kayf_Default(void);

public:
	std::vector<Box<SCALAR>*> operator()(Box<SCALAR>* argp_b, std::vector<std::string> arg_varNames, SCALAR arg_boxGap);

};


// ######################## //


//
template<typename SCALAR>
Kayf_Default<SCALAR>::Kayf_Default(void)
{
}


//
template<typename SCALAR>
Kayf_Default<SCALAR>::~Kayf_Default(void)
{
}


//
template<typename SCALAR>
std::vector<Box<SCALAR>*> Kayf_Default<SCALAR>::operator()(Box<SCALAR>* argp_b, std::vector<std::string> arg_varNames, SCALAR arg_boxGap)
{
	// prepare the result
	std::vector<Box<SCALAR>*> res;

	// we assume only one variable to split
	int arg_indice	= argp_b->box_params->vToC->at(arg_varNames[0]);

	// split variable interval into how many intervals?
	int nb_boxes	= 2;

	for(int i = 0; i < nb_boxes; i++)
	{
		SCALAR width_new_boxes	= argp_b->width[arg_indice] / (SCALAR)nb_boxes;
		SCALAR inf				= argp_b->lower[arg_indice];
		//double sup				= argp_b->upper[arg_indice];

		Box<SCALAR>* b			= new Box<SCALAR>(argp_b);
		
		SCALAR new_infBound		= inf + width_new_boxes * (SCALAR)i;
		SCALAR new_supBound		= inf + width_new_boxes * (SCALAR)(i + 1);// + (SCALAR)0.01;
		//SCALAR new_supBound		= inf + width_new_boxes * (SCALAR)(i + 1) + (SCALAR)0.01;

		// make an inflation of the polytope, we try with 5%
		new_infBound -= (SCALAR)arg_boxGap * argp_b->width[arg_indice];
		new_supBound += (SCALAR)arg_boxGap * argp_b->width[arg_indice];

		// update the bounds of the new box using box methods
		b->updateBound(-1, arg_varNames[0], new_infBound);
		b->updateBound(1, arg_varNames[0], new_supBound);

		//b->lower[arg_indice]	= inf + width_new_boxes * i;
		//b->upper[arg_indice]	= inf + width_new_boxes * (i + 1);

		// update width of new box
		//b->width[arg_indice]	= width_new_boxes;

		// stability may have changed during this step, so reinitialize
		b->lower_stability[arg_indice]	= 0;
		b->upper_stability[arg_indice]	= 0;

		res.push_back(b);
	}

	return res;
}


} // namespace bbs


#endif
