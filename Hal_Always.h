#ifndef _HAL_ALWAYS_H_
#define _HAL_ALWAYS_H_

#include "Box.h"

namespace bbs
{


template<typename SCALAR>
class Hal_Always
{

public:
	Hal_Always(void);
	~Hal_Always(void);

public:
	bool operator()(Box<SCALAR>* b);

};


// ############################### //


//
template<typename SCALAR>
Hal_Always<SCALAR>::Hal_Always(void)
{
}


//
template<typename SCALAR>
Hal_Always<SCALAR>::~Hal_Always(void)
{
}


// do we have to split, or not?
template<typename SCALAR>
bool Hal_Always<SCALAR>::operator()(Box<SCALAR>* argp_b)
{
	return true;
}


} // namespace bbs

#endif

