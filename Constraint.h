#ifndef _CONSTRAINT_H_
#define _CONSTRAINT_H_

/*!
 * \file		LPFactory.h
 * \brief		Programme de tests.
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * Programme de test pour l'objet de gestion des cha�nes de caract�res Str_t.
 *
 */

#include "Multivariate_Polynomial.h"

/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */

namespace bbs
{

enum constraint_type {CONSTRAINT_LESS_THAN,	CONSTRAINT_MORE_THAN, CONSTRAINT_EQUAL_TO};

/*! 
 * \class	LPFactory
 * \brief	Class responsible for the creation of LPs from the polynomial system
 *
 *  This class is responsible for creating LPs from polynomial systems. It is a kind of interface between these two modules (i.e. Polynomial System and LP modules)
 */

template<typename SCALAR>
class Constraint
{

public:
	/*!
     * \brief Default constructor
	 *
     */
	Constraint(void);

	/*!
     * \brief Constructeur
     *
     * This function is responsible for creating the LP model for a given system, on a given box, and with the LP representation given as template parameter
     *
     */
	Constraint(std::string arg_name, int arg_type, SCALAR arg_rightSide);

	/*!
     * \brief Constructeur
     *
     * This function is responsible for creating the LP model for a given system, on a given box, and with the LP representation given as template parameter
     *
     */
	Constraint(std::string arg_name, int arg_type, Multivariate_Polynomial<SCALAR> arg_leftSide, SCALAR arg_rightSide);

	/*!
     * \brief Constructeur
     *
     * This function is responsible for creating the LP model for a given system, on a given box, and with the LP representation given as template parameter
	 *
     */
	~Constraint(void);

// member variables
public:
	std::string			name;											// name of the contraint
	int					type;											// type of constraint

	Multivariate_Polynomial<SCALAR> leftSide;							// 
	SCALAR rightSide;													// right side of the constraint	

// methods
public:
	bool operator<(Constraint& other);									// operator

public:
	/*!
     * \brief Multivariate_Monomial output operator
     *
     * \param argr_o	: Output stream
	 * \param argr_mm	: Monomial to output
	 *
	 * \return			: Output stream
     */
	template<typename SCALAR>
	friend std::ostream& operator<< (std::ostream& argr_o, const Constraint<SCALAR>& argr_c);  // output operator

	template<typename SCALAR>
	friend std::istream& operator>> (std::istream& argr_i, Constraint<SCALAR>& argr_c);        // input operator

};


// ############################ //


template<typename SCALAR>
Constraint<SCALAR>::Constraint()
{

}


//
template<typename SCALAR>
Constraint<SCALAR>::Constraint(std::string arg_name, int arg_type, SCALAR arg_rightSide)
{
	this->name			= arg_name;
	this->type			= arg_type;
	this->leftSide		= Multivariate_Polynomial<SCALAR>();
	this->rightSide		= arg_rightSide;
}


//
template<typename SCALAR>
Constraint<SCALAR>::Constraint(std::string arg_name, int arg_type, Multivariate_Polynomial<SCALAR> arg_leftSide, SCALAR arg_rightSide)
{
	this->name			= arg_name;
	this->type			= arg_type;
	this->leftSide		= arg_leftSide;
	this->rightSide		= arg_rightSide;
}


//
template<typename SCALAR>
Constraint<SCALAR>::~Constraint(void)
{
}


//
template<typename SCALAR>
bool Constraint<SCALAR>::operator<(Constraint& other)
{
	return this->name < other.name;
}


// print a system
template<typename SCALAR>
std::ostream& operator<< (std::ostream& argr_o, const Constraint<SCALAR>& argr_c)
{

	//
	if(argr_c.type == CONSTRAINT_LESS_THAN)
	{
		argr_o << "0 >= ";
	}
	else if(argr_c.type == CONSTRAINT_MORE_THAN)
	{
		argr_o << "0 <= ";
	}
	else if(argr_c.type == CONSTRAINT_EQUAL_TO)
	{
		argr_o << "0 = ";
	}

	//
	argr_o << argr_c.leftSide;

	if(argr_c.rightSide != (SCALAR)0)
	{
		//
		argr_o << " -" << (argr_c.rightSide);
	}

	return argr_o;
}


// nothing special to do...
template<typename SCALAR>
std::istream& operator>> (std::istream& argr_i, Constraint<SCALAR>& argr_c)
{
	return argr_i;
}


} // namespace bbs


#endif