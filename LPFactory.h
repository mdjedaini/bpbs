#ifndef _LPFACTORY_H_
#define _LPFACTORY_H_

/*!
 * \file		LPFactory.h
 * \brief		LPFactory class
 * \author		Mahfoud DJEDAINI
 * \version		0
 * \date		2013/09/30
 *
 * File containing the LPFactory class, responsible for generating LP problems.
 *
 */

#include "System.h"
#include "Box.h"
//#include "Bernstein.h"


/*!
 * \namespace	bbs
 *
 * Namespace containing the code for the Bernstein polytopes-based solver
 *
 */
namespace bbs
{

// forward declare the Bernstein class
template<typename LP_PROBLEM, typename SCALAR>
class Bernstein;


/*! 
 * \class	LPFactory
 * \brief	Class responsible for the creation of LPs from the polynomial system
 *
 *  This class is responsible for creating LPs from polynomial systems. It is a kind of interface between these two modules (i.e. Polynomial System and LP modules)
 */
template<typename LP_PROBLEM, typename SCALAR>
class LPFactory
{

public:
	/*!
     * \brief Default constructor
     *
     */
	typedef	LP_PROBLEM	internal_lp_problem;

public:

	/*!
     * \brief Default constructor
     *
     */
	LPFactory();

	/*!
     * \brief Constructor from system
	 *
	 * \param argp_s		: Pointer to the system
     *
     */
	LPFactory(System<SCALAR>* argp_s);

	/*!
     * \brief Destructor
	 *
     */
	~LPFactory();

public:

	/*!
     * \brief Add a monomial to the LP factory
     *
     * \param argp_mm		: Pointer to the monomial to add
	 *
     */
	System<SCALAR>* s;

	/*!
     * \brief Add a monomial to the LP factory
     *
     * \param argp_mm		: Pointer to the monomial to add
	 *
     */
	std::map<int, Multivariate_Monomial<SCALAR>*>	mm_list;	/*!< List of monomials occuring in the system */

	/*!
     * \brief Add a monomial to the LP factory
     *
     * \param argp_mm		: Pointer to the monomial to add
	 *
     */
	std::map<std::string, int> mmToC;	/*!< Mapping between monomials and their Ids: monomial -> Id */
	
	/*!
     * \brief Add a monomial to the LP factory
     *
     * \param argp_mm		: Pointer to the monomial to add
	 *
     */
	std::map<int, std::string> cToMm;	/*!< Mapping between monomials and their Ids: Id -> monomial */

	/*!
     * \brief Add a monomial to the LP factory
     *
     * \param argp_mm		: Pointer to the monomial to add
	 *
     */
	std::map<int, std::pair<SCALAR, SCALAR>>	mm_bounds;	/*!< List of monomials occuring in the system */

	/*!
     * \brief Add a monomial to the LP factory
     *
     * \param argp_mm		: Pointer to the monomial to add
	 *
     */
	int nb_mm;

//
public:

	/*!
     * \brief Add a monomial to the LP factory
     *
     * \param argp_mm		: Pointer to the monomial to add
	 *
     */
	void addMonomial(Multivariate_Monomial<SCALAR>* argp_mm);


	/*!
     * \brief Add a monomial to the LP factory
     *
     * \param argp_mm		: Pointer to the monomial to add
	 *
     */
	void addConstraintToLPProblem(LP_PROBLEM* argp_lp, Constraint<SCALAR>* argp_c);


	/*!
     * \brief Constructeur
     *
     * This function is responsible for creating the LP model for a given system, on a given box, and with the LP representation given as template parameter
     *
     * \param argp_s		: Pointer on the system for which we want to create the LP model
	 * \param argp_box		: Pointer on the box for which we want to create the LP model
	 *
	 * \return				: Pointer to the LP model created
	 *
     */
	LP_PROBLEM* createLPModel(System<SCALAR>* argp_s, Box<SCALAR>* argp_box);

	/*!
     * \brief Constructeur
     *
     * This function is responsible for creating the LP model for a given system, on a given box, and with the LP representation given as template parameter
     *
     * \param argp_s		: Pointer on the system for which we want to create the LP model
	 * \param argp_box		: Pointer on the box for which we want to create the LP model
	 *
	 * \return				: Pointer to the LP model created
	 *
     */
	LP_PROBLEM* buildLP(LP_PROBLEM* argp_model);

	/*!
     * \brief Compute bounds of a given monomial on a given box
     *
     * This function is responsible for creating the LP model for a given system, on a given box, and with the LP representation given as template parameter
     *
     * \param argp_s		: Pointer on the system for which we want to create the LP model
	 * \param argp_box		: Pointer on the box for which we want to create the LP model
	 *
	 * \return				: Pointer to the LP model created
	 *
     */
	std::pair<SCALAR, SCALAR> computeMmBounds(System<SCALAR>* argp_s, Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_b);

	/*!
     * \brief Creates submonomials for a given monomial
     *
     * This function is responsible for creating the LP model for a given system, on a given box, and with the LP representation given as template parameter
     *
     * \param argp_mm		: Pointer to a multivariate monomial
	 *
	 * \return				: List of submonomials of the argument monomial
	 *
     */
	std::vector<Multivariate_Monomial<SCALAR>*> getSubMonomials(Multivariate_Monomial<SCALAR>* argp_mm);

};


} // namespace bbs


// ######################### //


#include "Bernstein.h"


namespace bbs
{


//
template<typename LP_PROBLEM, typename SCALAR>
LPFactory<LP_PROBLEM, SCALAR>::LPFactory()
{
	this->nb_mm	= 0;
}


//
template<typename LP_PROBLEM, typename SCALAR>
LPFactory<LP_PROBLEM, SCALAR>::LPFactory(System<SCALAR>* argp_s)
{
	this->s	= argp_s;
	this->nb_mm	= 0;
}


//
template<typename LP_PROBLEM, typename SCALAR>
LPFactory<LP_PROBLEM, SCALAR>::~LPFactory()
{
}


//
template<typename LP_PROBLEM, typename SCALAR>
void LPFactory<LP_PROBLEM, SCALAR>::addMonomial(Multivariate_Monomial<SCALAR>* argp_mm)
{
	// if we do not find the monomial, we add it
	if(this->mmToC.find(argp_mm->name) == this->mmToC.end())
	{
		this->mm_list[this->nb_mm]	= argp_mm;

		this->mmToC[argp_mm->name]	= this->nb_mm;
		this->cToMm[this->nb_mm]	= argp_mm->name;

		this->nb_mm++;
	}
}


//
template<typename LP_PROBLEM, typename SCALAR>
void LPFactory<LP_PROBLEM, SCALAR>::addConstraintToLPProblem(LP_PROBLEM* argp_lp, Constraint<SCALAR>* argp_c)
{
	// compute the ids
	int size	= argp_c->leftSide.mm_list.size();
	std::vector<int> mm_id_list(size);

	for(int i = 0; i < size; i++)
	{
		mm_id_list[i]	= this->mmToC[(argp_c->leftSide.mm_list[i])->name];
	}

	// add the constraint
	argp_lp->addConstraint(argp_c->name, 0, argp_c->type, mm_id_list, argp_c->leftSide.mm_coeff_list, argp_c->rightSide);
}


// This function is responsible for the creation of a LP model from a system
// It is a kind of interface between these two components (ie System and LP)
template<typename LP_PROBLEM, typename SCALAR>
LP_PROBLEM* LPFactory<LP_PROBLEM, SCALAR>::createLPModel(System<SCALAR>* argp_s, Box<SCALAR>* argp_box)
{
	// Import monomials from the system only one time
	if(this->mm_list.size() == 0)
	{

		// copy system monomials into mm_list
		std::map<int, Multivariate_Monomial<SCALAR>>::iterator it_mm;
		for(it_mm = argp_s->mm_list.begin(); it_mm != argp_s->mm_list.end(); it_mm++)
		{
			this->addMonomial(&(it_mm->second));
		}
	
		
		// create sub monomials for each monomial of the system
		// std::vector<Multivariate_Monomial<SCALAR>>::iterator it_mm;
		for(it_mm = argp_s->mm_list.begin(); it_mm != argp_s->mm_list.end(); it_mm++)
		{
			// get submonomials for the current msystem monomial
			std::vector<Multivariate_Monomial<SCALAR>*> tmp_mm_list	= this->getSubMonomials(&(it_mm->second));
	
			// for each monomial
			for(unsigned int i = 0; i < tmp_mm_list.size(); i++)
			{
				this->addMonomial(tmp_mm_list[i]);
			}

		}

	}

	// compute monomial bounds for the current box
	std::map<int, Multivariate_Monomial<SCALAR>*>::iterator it_mm;
	for(it_mm = this->mm_list.begin(); it_mm != this->mm_list.end(); it_mm++)
	{
		this->computeMmBounds(argp_s, it_mm->second, argp_box);
	}

	// create a bernstein with data
	Bernstein<LP_PROBLEM, SCALAR>	b	= Bernstein<LP_PROBLEM, SCALAR>(this);

	// create the bernstein inequalities from the box for all the monomials in the system
	std::vector<Constraint<SCALAR>*> b_list	= b.buildConstraints(&(this->mm_list), argp_box);

	int nbv		= this->mm_list.size();
	int nbc		= argp_s->c_list.size() + b_list.size();

	// instantiate the lp model
	LP_PROBLEM* lp_tmp	= new LP_PROBLEM(nbv, nbc);

	// add variables to the lp model, in fact they are monomials
	// use the same iterator as previously to avoid declaring a new one
	//std::map<int, Multivariate_Monomial<SCALAR>*>::iterator it_mm;
	for(it_mm = this->mm_list.begin(); it_mm != this->mm_list.end(); it_mm++)
	{
		// get the monomial Id
		int varId	= it_mm->first;

		// add the current monomial to the lp model as a variable
		lp_tmp->addVariable(this->cToMm[varId], varId, (this->mm_bounds.operator[](varId)).first, (this->mm_bounds.operator[](varId)).second);
	}

	// add constraints from the system
	std::vector<Constraint<SCALAR>>::iterator it_c;
	for(it_c = argp_s->c_list.begin(); it_c != argp_s->c_list.end(); it_c++)
	{
		//cout << *it_c << endl;

		// we provide the mapping with the constraint
		this->addConstraintToLPProblem(lp_tmp, &(*it_c));
	}

	

	// add all the bernstein inequalities to the lp model
	std::vector<Constraint<SCALAR>*>::iterator it_b;
	for(it_b = b_list.begin(); it_b != b_list.end(); it_b++)
	{
		//cout << **it_b << endl;

		this->addConstraintToLPProblem(lp_tmp, (*it_b));
	}

	// return the lp model
	return lp_tmp;
}


// TODO ne pas creer les 2n LPs, mais uniquement pour les variables qui en ont besoin
template<typename LP_PROBLEM, typename SCALAR>
LP_PROBLEM* LPFactory<LP_PROBLEM, SCALAR>::buildLP(LP_PROBLEM* argp_model)
{
	// the result is a copy of the model
	LP_PROBLEM* res	= new LP_PROBLEM(argp_model);

	// 
	return res;
}


// compute bounds of a monomial for a given box
template<typename LP_PROBLEM, typename SCALAR>
std::pair<SCALAR, SCALAR> LPFactory<LP_PROBLEM, SCALAR>::computeMmBounds(System<SCALAR>* argp_s, Multivariate_Monomial<SCALAR>* argp_mm, Box<SCALAR>* argp_b)
{
	// TODO
	std::pair<SCALAR, SCALAR> res;

	// degree 1 and only 1 variable, so we have a simple monomial (i.e. a simple variable)
	if(argp_mm->degree == 1 && argp_mm->factors_list.size() == 1)
	{
		// get the id of the monomial
		int varId	= this->mmToC[argp_mm->name];
		
		res.first	= argp_b->lower[varId];		// lower bound
		res.second	= argp_b->upper[varId];		// upper bound
	}

	// degree 2 and only one variable, so it is square variable
	else if(argp_mm->degree == 2 && argp_mm->factors_list.size() == 1)
	{
		// get the id of the variable
		int varId	= this->mmToC[std::get<0>(*(argp_mm->factors_list.begin()))];

		SCALAR inf_v	= argp_b->lower[varId];		// lower bound
		SCALAR sup_v	= argp_b->upper[varId];		// upper bound
		
		std::vector<SCALAR> data;
		
		data.push_back(inf_v * inf_v);
		data.push_back(sup_v * sup_v);

		if(inf_v < (SCALAR)0 && sup_v > (SCALAR)0)
		{
			res.first	= (SCALAR)0;
			res.second	= *std::max_element(data.begin(), data.end());
		}
		else
		{
			res.first	= *std::min_element(data.begin(), data.end());
			res.second	= *std::max_element(data.begin(), data.end());
		}
	}

	// degree 2 and exactly two variables, so it is mixed variable
	else if(argp_mm->degree == 2 && argp_mm->factors_list.size() == 2)
	{
		// get the id of the variable
		int var1Id	= this->mmToC[std::get<0>(*(argp_mm->factors_list.begin()))];
		int var2Id	= this->mmToC[std::get<0>(*(argp_mm->factors_list.rbegin()))];
		
		// get the bounds
		SCALAR inf_v1	= argp_b->lower[var1Id];		// lower bound
		SCALAR sup_v1	= argp_b->upper[var1Id];		// upper bound
		SCALAR inf_v2	= argp_b->lower[var2Id];		// lower bound
		SCALAR sup_v2	= argp_b->upper[var2Id];		// upper bound
		
		std::vector<SCALAR> data;
		
		data.push_back(inf_v1 * inf_v2);
		data.push_back(inf_v1 * sup_v2);
		data.push_back(sup_v1 * inf_v2);
		data.push_back(sup_v1 * sup_v2);
		
		// get the bounds
		res.first	= *std::min_element(data.begin(), data.end());
		res.second	= *std::max_element(data.begin(), data.end());
	}

	// degree 3 and one variable, eg x^3
	else if(argp_mm->degree == 3 && argp_mm->factors_list.size() == 1)
	{
		// get the id of the variable
		int varId	= this->mmToC[std::get<0>(*(argp_mm->factors_list.begin()))];
		
		// get the bounds
		SCALAR inf_v	= argp_b->lower[varId];		// lower bound
		SCALAR sup_v	= argp_b->upper[varId];		// upper bound	
		
		std::vector<SCALAR> data;
		
		data.push_back(inf_v * inf_v * inf_v);
		data.push_back(sup_v * sup_v * sup_v);
		
		// get the bounds
		res.first	= *std::min_element(data.begin(), data.end());
		res.second	= *std::max_element(data.begin(), data.end());
	}

	// degree 3 and two variables, eg x^2y
	else if(argp_mm->degree == 3 && argp_mm->factors_list.size() == 2)
	{
		// iterator on factors list
		std::set<std::tuple<std::string, int>>::iterator it_factor	= argp_mm->factors_list.begin();

		// get the id of the variable, such as var1Id is the degree 2 variable
		int var1Id, var2Id;
		
		// if degree of first variable is 2, then var1Id is the first factor
		if(std::get<1>(*it_factor) == 2)
		{
			var1Id	= this->mmToC[std::get<0>(*it_factor)];
			it_factor++;
			var2Id	= this->mmToC[std::get<0>(*it_factor)];
		}
		// or else, var1Id is the second factor of the list
		else
		{
			var2Id	= this->mmToC[std::get<0>(*it_factor)];
			it_factor++;
			var1Id	= this->mmToC[std::get<0>(*it_factor)];
		}

		// here, var1 is square factor, whereas var2 is simple factor

		// get the bounds
		SCALAR inf_v1	= argp_b->lower[var1Id];		// lower bound
		SCALAR sup_v1	= argp_b->upper[var1Id];		// upper bound
		SCALAR inf_v2	= argp_b->lower[var2Id];		// lower bound
		SCALAR sup_v2	= argp_b->upper[var2Id];		// upper bound

		// VAR1 BOUNDS COMPUTATION
		std::vector<SCALAR> data_square;
		
		// bounds of square factor
		SCALAR inf_square, sup_square;

		data_square.push_back(inf_v1 * inf_v1);
		data_square.push_back(sup_v1 * sup_v1);

		if(inf_v1 < (SCALAR)0 && sup_v1 > (SCALAR)0)
		{
			inf_square	= (SCALAR)0;
			sup_square	= *std::max_element(data_square.begin(), data_square.end());
		}
		else
		{
			inf_square	= *std::min_element(data_square.begin(), data_square.end());
			sup_square	= *std::max_element(data_square.begin(), data_square.end());
		}

		std::vector<SCALAR> data;
		
		data.push_back(inf_square * inf_v2);
		data.push_back(inf_square * sup_v2);
		data.push_back(sup_square * inf_v2);
		data.push_back(sup_square * sup_v2);
		
		// get the bounds
		res.first	= *std::min_element(data.begin(), data.end());
		res.second	= *std::max_element(data.begin(), data.end());
	}

	// degree 3 and exactly three variables, eg x*y*z
	else if(argp_mm->degree == 3 && argp_mm->factors_list.size() == 3)
	{
		// iterator on factors list
		std::set<std::tuple<std::string, int>>::iterator it_factor	= argp_mm->factors_list.begin();

		// get the id of the variable
		int var1Id	= this->mmToC[std::get<0>(*it_factor)];
		it_factor++;
		int var2Id	= this->mmToC[std::get<0>(*it_factor)];
		it_factor++;
		int var3Id	= this->mmToC[std::get<0>(*it_factor)];
		
		// get the bounds
		SCALAR inf_v1	= argp_b->lower[var1Id];		// lower bound
		SCALAR sup_v1	= argp_b->upper[var1Id];		// upper bound
		SCALAR inf_v2	= argp_b->lower[var2Id];		// lower bound
		SCALAR sup_v2	= argp_b->upper[var2Id];		// upper bound
		SCALAR inf_v3	= argp_b->lower[var3Id];		// lower bound
		SCALAR sup_v3	= argp_b->upper[var3Id];		// upper bound

		std::vector<SCALAR> data;
		
		data.push_back(inf_v1 * inf_v2 * inf_v3);
		data.push_back(inf_v1 * inf_v2 * sup_v3);
		data.push_back(inf_v1 * sup_v2 * inf_v3);
		data.push_back(inf_v1 * sup_v2 * sup_v3);
		data.push_back(sup_v1 * inf_v2 * inf_v3);
		data.push_back(sup_v1 * inf_v2 * sup_v3);
		data.push_back(sup_v1 * sup_v2 * inf_v3);
		data.push_back(sup_v1 * sup_v2 * sup_v3);
		
		// get the bounds
		res.first	= *std::min_element(data.begin(), data.end());
		res.second	= *std::max_element(data.begin(), data.end());
	}

	// TODO
	// monomials with degree higher than 3 (for the moment...)
	else
	{
		// degree non managed
		std::cout << "Bounds computation not managed for monomials with degree > 2 ..." << endl;
	}

	// add bounds to bounds list
	this->mm_bounds[this->mmToC[argp_mm->name]]	= res;

	// TODO, remove the return here if not needed
	// return the bounds, may be we will need it
	return res;
}


// get submonomials of the argument monomial
template<typename LP_PROBLEM, typename SCALAR>
std::vector<Multivariate_Monomial<SCALAR>*> LPFactory<LP_PROBLEM, SCALAR>::getSubMonomials(Multivariate_Monomial<SCALAR>* argp_mm)
{
	// TODO
	std::vector<Multivariate_Monomial<SCALAR>*> res;

	// if degree equals 1 or 2, no new sub monomial is required
	if(argp_mm->degree == 1 || argp_mm->degree == 2)
	{
	}

	// if monomial degree is 3 and the monomial has 1 variable, eg x^3, we have to add the monomial x^2
	else if(argp_mm->degree == 3 && argp_mm->factors_list.size() == 1)
	{
		// iterator on factors list
		std::set<std::tuple<std::string, int>>::iterator it_factor	= argp_mm->factors_list.begin();

		// get the name of the base variable, for example, if monomial is x^3, the name is x
		std::string varName	= std::get<0>(*it_factor);

		// create the monomial x^2 , and add it to the results
		Multivariate_Monomial<SCALAR>* mm_tmp	= new Multivariate_Monomial<SCALAR>(varName, 2);
		res.push_back(mm_tmp);
	}

	// if monomial degree is 3 and the monomial has 2 variable, eg x*y^2, we have to add the monomials x*y and y^2
	else if(argp_mm->degree == 3 && argp_mm->factors_list.size() == 2)
	{
		// iterator on factors list
		std::set<std::tuple<std::string, int>>::iterator it_factor	= argp_mm->factors_list.begin();

		// get the names such as var_y is the degree 2 variable
		std::string var_x, var_y;
		
		// if degree of first factor is 2, then var_y is the first factor
		if(std::get<1>(*it_factor) == 2)
		{
			var_y	= std::get<0>(*it_factor);
			it_factor++;
			var_x	= std::get<0>(*it_factor);
		}
		// or else, var_y is the second factor of the list
		else
		{
			var_x	= std::get<0>(*it_factor);
			it_factor++;
			var_y	= std::get<0>(*it_factor);
		}

		// create the monomial xy, and add it to the results
		Multivariate_Monomial<SCALAR>* mm_xy	= new Multivariate_Monomial<SCALAR>(var_x, 1);
		mm_xy->addFactor(var_y, 1);
		res.push_back(mm_xy);

		// create the monomial x^2 , and add it to the results
		Multivariate_Monomial<SCALAR>* mm_y2	= new Multivariate_Monomial<SCALAR>(var_y, 2);
		res.push_back(mm_y2);
	}

	// if monomial degree is 3 and the monomial has 3 variable, eg x*y*z, we have to add the monomials x*y, x*z and y*z
	else if(argp_mm->degree == 3 && argp_mm->factors_list.size() == 3)
	{
		// iterator on factors list
		std::set<std::tuple<std::string, int>>::iterator it_factor	= argp_mm->factors_list.begin();

		// get the id of the variable, such as var1Name is the degree 2 variable
		std::string var_x, var_y, var_z;
		
		var_x	= std::get<0>(*it_factor);
		it_factor++;
		var_y	= std::get<0>(*it_factor);
		it_factor++;
		var_z	= std::get<0>(*it_factor);

		// create the monomial x*y , and add it to the results
		Multivariate_Monomial<SCALAR>* mm_xy	= new Multivariate_Monomial<SCALAR>(var_x, 1);
		mm_xy->addFactor(var_y, 1);
		res.push_back(mm_xy);

		// create the monomial x*z , and add it to the results
		Multivariate_Monomial<SCALAR>* mm_xz	= new Multivariate_Monomial<SCALAR>(var_x, 1);
		mm_xz->addFactor(var_z, 1);
		res.push_back(mm_xz);

		// create the monomial y*z , and add it to the results
		Multivariate_Monomial<SCALAR>* mm_yz	= new Multivariate_Monomial<SCALAR>(var_y, 1);
		mm_yz->addFactor(var_z, 1);
		res.push_back(mm_yz);

	}

	else
	{
		// manage here monomials of degree > 2
		cout << "Do not forget to add sub monomials for monomial: " << argp_mm->name << endl;
	}

	return res;
}

} // namespace bbs


#endif